﻿using DAL.Interfaces;

namespace BLL.BusinessLogics
{
    public class UsersOrdersDetailBL
    {
        private readonly IGroc_OrderRepository orderRepo;
        private readonly IHandy_RequestsRepository handyRepo;
        private readonly IInterCountryDeliveryRepository deliveryRepo;

        public UsersOrdersDetailBL(IGroc_OrderRepository _orderRepo, IHandy_RequestsRepository _handyRepo,IInterCountryDeliveryRepository _deliveryRepo)
        {
            orderRepo = _orderRepo;
            handyRepo = _handyRepo;
            deliveryRepo = _deliveryRepo;
        }

        public int GetPendingOrdersCount(int userid,bool isGuestUser,string deviceId, string[] phoneNumbers = null)
        {
            var pendingOrdersCount = 0;         
            var pendingServicesCount = 0;    
            var pendingDeliveriesCount = 0;  
            if (!isGuestUser)
            {
                pendingOrdersCount = orderRepo.GetPendingGrocerryOrdersCount(userid);
                pendingServicesCount = handyRepo.GetPendingHandyReqsCount(userid);
                pendingDeliveriesCount = deliveryRepo.GetPendingDeliveryReqsCount(userid);
            }
            else
            {
                if(phoneNumbers.Length > 0)
                {
                    pendingOrdersCount = orderRepo.GetGuestPendingGrocerryOrdersByPhoneNumbersCount(phoneNumbers);
                    pendingServicesCount = handyRepo.GetGuestPendingHandyReqsByPhoneNumbersCount(phoneNumbers);
                    pendingDeliveriesCount = deliveryRepo.GetGuestPendingDeliveryReqsByPhoneNumbersCount(phoneNumbers);
                }
                else
                {
                    pendingOrdersCount = orderRepo.GetGuestPendingGrocerryOrdersByDeviceIdCount(deviceId);
                    pendingServicesCount = handyRepo.GetGuestPendingHandyReqsByDeviceIdCount(deviceId);
                    pendingDeliveriesCount = deliveryRepo.GetGuestPendingDeliveryReqsByDeviceIdCount(deviceId);
                }
             
            }
            var totalPendingOrdercount = pendingOrdersCount;

            if (pendingServicesCount > 0)
            {
                totalPendingOrdercount += pendingServicesCount;
            }

            return totalPendingOrdercount + pendingDeliveriesCount;
        }





    }
}
