﻿using BOL.Models;
using DAL.Interfaces;
using System.Linq;

namespace BLL.BusinessLogics
{
    public class Groc_ProductsBL
    {
        private readonly IGroc_ProductsRepository grocProductsRepo;

        public Groc_ProductsBL(IGroc_ProductsRepository _grocProductsRepo)
        {
            grocProductsRepo = _grocProductsRepo;
        }

        public IQueryable<Groc_Items> GetAllProdcuts()
        {
            var tenders = grocProductsRepo.GetAll();
            return tenders;
        }
    }
}
