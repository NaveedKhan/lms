﻿using BLL.ViewModels;
using BOL.DBContext;
using BOL.Enums;
using BOL.Models;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BLL.BusinessLogics
{
    public class Groc_OrdersBL
    {
        private readonly IGroc_OrderRepository orderRepo;

        public Groc_OrdersBL(IGroc_OrderRepository _orderRepo)
        {
            orderRepo = _orderRepo;
        }


        public IQueryable<Groc_Order> GetAllOrders()
        {
            var tenders = orderRepo.GetAll();
            return tenders;
        }
        public Task<Groc_Order> GetOrderbyId(int id)
        {
            var tenders = orderRepo.GetById(id);
            return tenders;
        }


        public List<OrdersHistoryViewModel> GePendingOrdersOfGuest(string[] phoneNumbers, AppDBContext _db, string Image404)
        {
            var ordersList = orderRepo.GetPendingOrdersOfGuest(phoneNumbers).AsEnumerable();
            var ordersHistroy = new List<OrdersHistoryViewModel>();
            if (ordersList.Any())
            {
                ordersHistroy = GetRelatedOrdersList(ordersList, _db, Image404);
            }
            return ordersHistroy;
        }

        public List<OrdersHistoryViewModel> GePendingOrdersOfGuestByDeviceId(string deviceId, AppDBContext _db, string Image404)
        {
            var ordersList = orderRepo.GetPendingOrdersOfGuestbyDeviceId(deviceId).AsEnumerable();
            var ordersHistroy = new List<OrdersHistoryViewModel>();
            if (ordersList.Any())
            {
                ordersHistroy = GetRelatedOrdersList(ordersList, _db, Image404);
            }
            return ordersHistroy;
        }


        public List<OrdersHistoryViewModel> GePendingOrdersOfUser(int userid, AppDBContext _db, string Image404)
        {
            var ordersList = orderRepo.GetPendingOrdersOfUser(userid).AsEnumerable();
            var ordersHistroy = new List<OrdersHistoryViewModel>();
            if (ordersList.Any())
            {
                ordersHistroy = GetRelatedOrdersList(ordersList, _db, Image404);
            }
            return ordersHistroy;
        }

        public List<OrdersHistoryViewModel> GetOrdersHistoryOfUser(int userid, AppDBContext _db, string Image404)
        {
            var ordersList = orderRepo.GetOrdersHistoryOfUser(userid).AsEnumerable();
            var ordersHistroy = new List<OrdersHistoryViewModel>();
            if (ordersList.Any())
            {
                ordersHistroy = GetRelatedOrdersList(ordersList, _db, Image404);
            }
            return ordersHistroy;
        }

        public List<OrdersHistoryViewModel> GetOrdersHistoryOfGuest(string[] phoneNumbers, AppDBContext _db, string Image404)
        {
            var ordersList = orderRepo.GetOrdersHistoryOfGuest(phoneNumbers).AsEnumerable();
            var ordersHistroy = new List<OrdersHistoryViewModel>();
            if (ordersList.Any())
            {
                ordersHistroy = GetRelatedOrdersList(ordersList, _db, Image404);
            }
            return ordersHistroy;
        }

        public List<OrdersHistoryViewModel> GetOrdersHistoryOfGuestByDeviceId(string deviceId, AppDBContext _db, string Image404)
        {
            var ordersList = orderRepo.GetOrdersHistoryOfGuestByDeviceID(deviceId).AsEnumerable();
            var ordersHistroy = new List<OrdersHistoryViewModel>();
            if (ordersList.Any())
            {
                ordersHistroy = GetRelatedOrdersList(ordersList, _db, Image404);
            }
            return ordersHistroy;
        }

        public int GetPendingOrdersCount(int userid)
        {
            var pendingOrdersCount = orderRepo.GetPendingGrocerryOrdersCount(userid);
            return pendingOrdersCount;
        }

        public int GetGuestPendingOrdersCount(string[] phoneNumbers)
        {
            var pendingOrdersCount = orderRepo.GetGuestPendingGrocerryOrdersByPhoneNumbersCount(phoneNumbers);
            return pendingOrdersCount;
        }

        public int GetGuestPendingOrdersByDeviceIdCount(string deviceId)
        {
           
            var pendingOrdersCount = orderRepo.GetGuestPendingGrocerryOrdersByDeviceIdCount(deviceId);
            return pendingOrdersCount;
        }

        public List<PendingOrdersDetail> GetPendingOrdersCountDetailOfUser(int userid,AppDBContext _db)
        {
            var pendingOrders = _db.Groc_Orders.Where(aaa => aaa.OrderById == userid && (aaa.OrderStatusID != (int)OrderStatus.BlackList
                       && aaa.OrderStatusID != (int)OrderStatus.Canceled && aaa.OrderStatusID != (int)OrderStatus.Delivered))
                       .Include(aa => aa.Groc_OrderStatus).Select(or => new PendingOrdersDetail
                       {
                           ServiceType = "Grocerry",
                           ServiceStatusName = or.Groc_OrderStatus.OrderStatusName
                       }).ToList();

            return pendingOrders;
        }

        public List<OrdersHistoryViewModel> GetAllNewOrdersRelatedToWorker(int workerId, AppDBContext _db,string Image404)
        {
            var ordersList = orderRepo.GetOrdersRelatedToWorker(workerId).AsEnumerable();
            var relatedOrders = new List<OrdersHistoryViewModel>();
            if (ordersList.Any())
            {
                relatedOrders = GetRelatedOrdersList(ordersList, _db, Image404);
            }
            return relatedOrders;
        }

        public async Task Update(int id, Groc_Order entity)
        {
            await orderRepo.Update(id, entity);
        }

        private List<OrdersHistoryViewModel> GetRelatedOrdersList(IEnumerable<Groc_Order> orders, AppDBContext _db, string Image404)
        {

            var orderids = orders.Select(aa => aa.OrderID).ToList();
            var relatedreviewsList = _db.Groc_Reviews.Where(aa => orderids.Contains(aa.OrderID));

            var orderItems = _db.Groc_OrderedItems.Include(aa => aa.Groc_Items)
                .ThenInclude(ss => ss.Groc_Units).Where(aa => orderids.Contains(aa.OrderID)).AsEnumerable();

            var ordersDetailList = new List<OrdersHistoryViewModel>();

            if (orders.Count() > 0 && orderItems.Count() > 0)
            {
                foreach (var ordr in orders)
                {
                    var orderitemslist = orderItems.Where(aa => aa.OrderID == ordr.OrderID).Select(ord => new OrderViewModel
                    {
                        ProdcutPriceEach = (int)ord.Groc_Items.ItemPrice,
                        ProductCount = ord.Quantity,
                        ProductId = ord.Groc_Items.ItemID,
                        ProductImage = string.IsNullOrEmpty(ord.Groc_Items.ImageName) ? Image404 : ord.Groc_Items.ImageName,
                        UnitName = ord.Groc_Items.Groc_Units.UnitName,
                        ProductName = ord.Groc_Items.ItemName,
                        UserId = ordr.OrderById,
                        TotalPrice = (int)ord.OrderItemAmount

                    }).ToList();
                    var requestrelatedreview = relatedreviewsList.FirstOrDefault(sa => sa.OrderID == ordr.OrderID);
                    var ordersDetail = new OrdersHistoryViewModel
                    {
                        RegionID = ordr.RegionID,
                        RegionName = ordr.RegionName,
                        OrderDate = ordr.OrderDate,
                        OrderID = ordr.OrderID,
                        OrderTotalItems = orderitemslist.Count,
                        OrderReEstimatedTotalCharges = ordr.OrderReEstimatedTotalCharges,
                        OrderTotalPrice = (int)ordr.OrderTotalCharges,
                        OrderStatus = ordr.Groc_OrderStatus.OrderStatusName,
                        OrderItems = orderitemslist.ToList(),
                        CurrentLocationAddress = ordr.CurrentLocationAddress,
                        CurrentLocationLat = ordr.CurrentLocationLat,
                        CurrentLocationLong = ordr.CurrentLocationLong,
                        IsScheduleOrder = ordr.IsScheduleOrder,
                        OrderByUserID = ordr.OrderById,
                        OrderCompletionDate = ordr.OrderCompletionDate,
                        OrderDetail = ordr.OrderDetail,
                        OrderScheduleDate = ordr.OrderScheduleDate,
                        OrderStatusID = ordr.OrderStatusID,
                        OrderTakenByID = ordr.OrderTakenBy,
                        OrderTotalCharges = ordr.OrderTotalCharges,
                        RequestAddressLat = ordr.RequestAddressLat,
                        RequestAddressLong = ordr.RequestAddressLong,
                        RequestLocationAddress = ordr.RequestLocationAddress,
                        OrderTakenByName = ordr.OrderTakenBy.ToString(),
                        RequesteeName = ordr.RequesteeName,
                        RequesteeContactNo = ordr.RequesteeContactNo,
                        LocationTrackingID = ordr.LocationTrackingID,
                        AssigneeName = ordr.AssigneeName,
                        AssigneContactNo = ordr.AssigneContactNo,
                        IsMarkFinishedByDriverTech = ordr.IsMarkFinishedByDriverTech,
                        IsMarkFinishedByUser = ordr.IsMarkFinishedByUser,
                        Reviews = requestrelatedreview,
                        OrderAssignedToDriverTechDate = ordr.OrderAssignedToDriverTechDate,
                        AssigneeImageURL = ordr.AssigneeImageURL,
                        ServiceCharges=ordr.ServiceCharges,
                        DeviceTocken = ordr.DeviceTocken,
                        DeviceID = ordr.DeviceID
                    };

                    ordersDetailList.Add(ordersDetail);
                }
            }
            return ordersDetailList;
        }


    }
}
