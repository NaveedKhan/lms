﻿using BLL.ViewModels;
using BOL.DBContext;
using BOL.Enums;
using BOL.Models;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace BLL.BusinessLogics
{
    public class Handy_RequestsBL
    {
        private readonly IHandy_RequestsRepository handyRepo;

        public Handy_RequestsBL(IHandy_RequestsRepository _handyRepo)
        {
            handyRepo = _handyRepo;
        }

        public int GetPendingHanyRequestsCount(int userid)
        {
            var pendingOrdersCount = handyRepo.GetPendingHandyReqsCount(userid);
            return pendingOrdersCount;
        }

        public int GetGuestPendingHanyRequestsCount(string[] phoneNumbers)
        {
            var pendingOrdersCount = handyRepo.GetGuestPendingHandyReqsByPhoneNumbersCount(phoneNumbers);
            return pendingOrdersCount;
        }

        public int GetGuestPendingHanyRequestsByDeviceIdCount(string deviceId)
        {
            var pendingOrdersCount = handyRepo.GetGuestPendingHandyReqsByDeviceIdCount(deviceId);
            return pendingOrdersCount;
        }

        public List<PendingOrdersDetail> GetPendingHanyRequestsCountDetailOfUser(int userid, AppDBContext _db)
        {
            var pendingServices = _db.Handy_ServiceRequests.Where(aa => aa.RequestById == userid
                       && (aa.ServiceStatusID != (int)ServiceStatus.Canceled && aa.ServiceStatusID != (int)ServiceStatus.Delivered
                       && aa.ServiceStatusID != (int)ServiceStatus.BlackList)).Include(sa => sa.Handy_ServiceStatus).Include(saa => saa.Handy_Servicetype)
                       .Select(se => new PendingOrdersDetail
                       {
                           ServiceType = se.Handy_Servicetype.SrviceTypeName,
                           ServiceStatusName = se.Handy_ServiceStatus.ServiceStatusName
                       }).ToList();
            return pendingServices;
        }

        public List<HandyServiceRequestVM> GetPendingHandyRequestsofUser(int userid, AppDBContext _db)
        {
            var requestsList = handyRepo.GetPendingRequestsOfUser(userid).AsEnumerable();
            var relatedRequests = new List<HandyServiceRequestVM>();
            if (requestsList.Any())
            {
                relatedRequests = GetRelatedRequesList(requestsList, _db);
            }
            return relatedRequests;
        }

        public List<HandyServiceRequestVM> GetPendingHandyRequestsofGuest(string[] phonenumbers, AppDBContext _db)
        {
            var requestsList = handyRepo.GetPendingRequestsOfGuest(phonenumbers).AsEnumerable();
            var relatedRequests = new List<HandyServiceRequestVM>();
            if (requestsList.Any())
            {
                relatedRequests = GetRelatedRequesList(requestsList, _db);
            }
            return relatedRequests;
        }


        public List<HandyServiceRequestVM> GetPendingHandyRequestsofGuestByDeviceId(string deviceId, AppDBContext _db)
        {
            var requestsList = handyRepo.GetPendingRequestsOfGuestbyDeviceId(deviceId).AsEnumerable();
            var relatedRequests = new List<HandyServiceRequestVM>();
            if (requestsList.Any())
            {
                relatedRequests = GetRelatedRequesList(requestsList, _db);
            }
            return relatedRequests;
        }


        public List<HandyServiceRequestVM> GetHandyRequestsHistoryofGuest(string[] phonenumbers, AppDBContext _db)
        {
            var requestsList = handyRepo.GetRequestsHistoryOfGuest(phonenumbers).AsEnumerable();
            var relatedRequests = new List<HandyServiceRequestVM>();
            if (requestsList.Any())
            {
                relatedRequests = GetRelatedRequesList(requestsList, _db);
            }
            return relatedRequests;
        }

        public List<HandyServiceRequestVM> GetHandyRequestsHistoryofGuestByDeviceId(string deviceId, AppDBContext _db)
        {
            var requestsList = handyRepo.GetRequestsHistoryOfGuestbyDeviceId(deviceId).AsEnumerable();
            var relatedRequests = new List<HandyServiceRequestVM>();
            if (requestsList.Any())
            {
                relatedRequests = GetRelatedRequesList(requestsList, _db);
            }
            return relatedRequests;
        }

        public List<HandyServiceRequestVM> GetHandyRequestsHistoryofUser(int userid, AppDBContext _db)
        {
            var requestsList = handyRepo.GetRequestsHistoryOfUser(userid).AsEnumerable();
            var relatedRequests = new List<HandyServiceRequestVM>();
            if (requestsList.Any())
            {
                relatedRequests = GetRelatedRequesList(requestsList, _db);
            }
            return relatedRequests;
        }

        public List<HandyServiceRequestVM> GetAllDedicatedRequestsOfWorker(int workerId, AppDBContext _db, List<string> roles)
        {
            var requestsList = handyRepo.GeDedicatedRequestsWorker(workerId, roles).AsEnumerable();

            var relatedRequests = new List<HandyServiceRequestVM>();
            if (requestsList.Any())
            {
                relatedRequests = GetRelatedRequesList(requestsList, _db);
            }
            return relatedRequests;
        }

        public List<HandyServiceRequestVM> GetAllRelatedRequestsOfWorker(int workerId, AppDBContext _db, List<string> roles)
        {
            var requestsList = handyRepo.GetRelatedRequestsWorker(workerId, roles).AsEnumerable();
            var relatedRequests = new List<HandyServiceRequestVM>();
            if (requestsList.Any())
            {
                relatedRequests = GetRelatedRequesList(requestsList, _db);
            }
            return relatedRequests;
        }


        private List<HandyServiceRequestVM> GetRelatedRequesList(IEnumerable<Handy_ServiceRequest> requests, AppDBContext _db)
        {

            var servicesIds = requests.Select(aa => aa.ServiceRequestID).ToList();
            var relatedreviewsList = _db.Groc_Reviews.Where(aa => servicesIds.Contains(aa.ServiceRequestID));
            var serviesOfRequests = _db.Handy_ServiceRequestServices.Where(aa => servicesIds.Contains(aa.RequestID))
                .Include(aas => aas.Handy_Services).ThenInclude(ss => ss.Groc_Units)
                .AsEnumerable();
            var handyReqeusts = new List<HandyServiceRequestVM>();
            foreach (var req in requests)
            {
                var relatedservices = serviesOfRequests.Where(aa => aa.RequestID == req.ServiceRequestID);
                var imagesarray = GetImagesAddresses(req);
                var requestrelatedreview = relatedreviewsList.FirstOrDefault(sa => sa.ServiceRequestID == req.ServiceRequestID);


                var requesstsVM = new HandyServiceRequestVM
                {
                    IsMarkFinishedByDriverTech = req.IsMarkFinishedByDriverTech,
                    IsMarkFinishedByUser = req.IsMarkFinishedByUser,
                    Reviews = requestrelatedreview,
                    RegionID = req.RegionID,
                    RegionName = req.RegionName,
                    CurrentLocationAddress = req.CurrentLocationAddress,
                    CurrentLocationLat = req.CurrentLocationLat,
                    CurrentLocationLong = req.CurrentLocationLong,
                    IsInstallation = req.IsInstallation,
                    IsMaintainance = req.IsMaintainance,
                    IsServiceSchedule = req.IsServiceSchedule,
                    RequestAddressLat = req.RequestAddressLat,
                    RequestAddressLong = req.RequestAddressLong,
                    RequestById = req.RequestById,
                    RequestImage1Address = req.RequestImage1Address,
                    RequestImage1Name = req.RequestImage1Name,
                    RequestImage2Address = req.RequestImage2Address,
                    RequestImage2Name = req.RequestImage2Name,
                    RequestImage3Address = req.RequestImage3Address,
                    RequestImage3Name = req.RequestImage3Name,
                    RequestImage4Address = req.RequestImage4Address,
                    RequestImage4Name = req.RequestImage4Name,
                    RequestImage5Address = req.RequestImage5Address,
                    RequestImage5Name = req.RequestImage5Name,
                    RequestImage6Address = req.RequestImage6Address,
                    RequestImage6Name = req.RequestImage6Name,
                    ServiceRequestClosingDate = req.ServiceRequestClosingDate,
                    RequestLocationAddress = req.RequestLocationAddress,
                    ServiceRequestComments = req.ServiceRequestComments,
                    ServiceRequestDate = req.ServiceRequestDate,
                    ServiceRequestDetail = req.ServiceRequestDetail,
                    ServiceRequestID = req.ServiceRequestID,
                    ServiceStatusID = req.ServiceStatusID,
                    // ServiceStatusName = req.Handy_ServiceStatus.ServiceStatusName,
                    ServiceRequestScheduleDate = req.ServiceRequestScheduleDate,
                    ServiceTakenBy = req.ServiceTakenBy,
                    ServiceTotalCharges = req.ServiceTotalCharges,
                    ServiceTypeID = req.ServiceTypeID,
                   // ServiceTypeName = req.Handy_Servicetype.SrviceTypeName,
                    RequesteeAddress = req.CurrentLocationAddress,
                    RequesteeContactNo = req.RequesteeContactNo,
                    RequesteeName = req.RequesteeName,
                    ImagesSourceArray = imagesarray,
                    ServiceList = relatedservices.ToList(),
                    LocationTrackingID = req.LocationTrackingID,
                    ServiceReEstimatedTotalCharges = req.ServiceReEstimatedTotalCharges,
                    AssigneContactNo = req.AssigneContactNo,
                    AssigneeImageURL = req.AssigneeImageURL,
                    AssigneeName = req.AssigneeName,
                    ServiceAssignedToDriverTechDate = req.ServiceAssignedToDriverTechDate,
                    ServiceCharges=req.ServiceCharges,
                    DeviceTocken = req.DeviceTocken,
                    DeviceID = req.DeviceID
                };

                if (req.Handy_ServiceStatus != null)
                {
                    requesstsVM.ServiceStatusName = req.Handy_ServiceStatus.ServiceStatusName;
                }
                else
                {
                    var staent = _db.Handy_ServiceStatuses.FirstOrDefault(sa => sa.ServiceStatusID == req.ServiceStatusID);
                    if (staent != null)
                    {
                        requesstsVM.ServiceStatusName = staent.ServiceStatusName;
                    }
                    else
                    {
                        requesstsVM.ServiceStatusName = "UnKnown";
                    }
                }

                if (req.Handy_Servicetype != null)
                {
                    requesstsVM.ServiceTypeName = req.Handy_Servicetype.SrviceTypeName;
                }
                else
                {
                    var staent = _db.Handy_Servicetypes.FirstOrDefault(sa => sa.ServiceTypeId == req.ServiceTypeID);
                    if (staent != null)
                    {
                        requesstsVM.ServiceTypeName = staent.SrviceTypeName;
                    }
                    else
                    {
                        requesstsVM.ServiceTypeName = "UnKnown";
                    }
                }

                handyReqeusts.Add(requesstsVM);
            }
            return handyReqeusts.OrderByDescending(aa => aa.ServiceRequestDate).ToList();
        }


        private List<string> GetImagesAddresses(Handy_ServiceRequest requests)
        {
            var ImagesSourceArray = new List<string>();
            if (!string.IsNullOrEmpty(requests.RequestImage1Address))
            {
                ImagesSourceArray.Add(requests.RequestImage1Address);
            }
            if (!string.IsNullOrEmpty(requests.RequestImage2Address))
            {
                ImagesSourceArray.Add(requests.RequestImage2Address);
            }
            if (!string.IsNullOrEmpty(requests.RequestImage3Address))
            {
                ImagesSourceArray.Add(requests.RequestImage3Address);
            }
            if (!string.IsNullOrEmpty(requests.RequestImage4Address))
            {
                ImagesSourceArray.Add(requests.RequestImage4Address);
            }
            if (!string.IsNullOrEmpty(requests.RequestImage5Address))
            {
                ImagesSourceArray.Add(requests.RequestImage5Address);
            }
            if (!string.IsNullOrEmpty(requests.RequestImage6Address))
            {
                ImagesSourceArray.Add(requests.RequestImage6Address);
            }

            return ImagesSourceArray;
        }

    }
}
