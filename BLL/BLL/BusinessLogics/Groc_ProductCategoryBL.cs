﻿using BOL.Models;
using DAL.Interfaces;
using System.Linq;

namespace BLL.BusinessLogics
{
    public class Groc_ProductCategoryBL
    {
        private readonly IGroc_ProductCategoryRepository grocProductsCatRepo;

        public Groc_ProductCategoryBL(IGroc_ProductCategoryRepository _grocProductsCatRepo)
        {
            grocProductsCatRepo = _grocProductsCatRepo;
        }

        public IQueryable<Groc_ItemCategory> GetAllProductsCategories()
        {
            var cat = grocProductsCatRepo.GetAll();
            return cat;
        }
    }
}
