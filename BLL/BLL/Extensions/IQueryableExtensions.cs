﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace BLL.Extensions
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> AddWhereClause<T>(this IQueryable<T> query, Expression<Func<T, bool>> where) where T : class
        {
            return query.Where(where);
        }
        public static IQueryable<T> AddOrderByClause<T>(this IQueryable<T> query, Expression<Func<T, object>> orderByExpr) where T : class
        {
            return query.OrderBy(orderByExpr);
        }
        public static IQueryable<T> AddOrderByDescendingClause<T>(this IQueryable<T> query, Expression<Func<T, object>> orderByExpr) where T : class
        {
            return query.OrderByDescending(orderByExpr);
        }
        public static IQueryable<T> IncludeProperties<T>(this IQueryable<T> query, params Expression<Func<T, object>>[] includeProperties) where T : class
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }
#pragma warning disable S107 // Methods should not have too many parameters
        public static IQueryable<T> AddFilterAndSortExpression<T>(this IQueryable<T> query, ref int count, Expression<Func<T, bool>> where = null, Expression<Func<T, object>> orderByExpr = null, Expression<Func<T, object>> orderByDes = null, bool isSortEnabled = false, bool isSortDes = false, params Expression<Func<T, object>>[] includeProperties) where T : class
        {
            if (includeProperties != null)
                query = query.IncludeProperties(includeProperties);
            if (where != null)
            {
                query = query.AddWhereClause(where);
                count = query.Count();
            }
            if (orderByExpr != null)
                query = query.AddOrderByClause(orderByExpr);
            if (orderByDes != null)
                query = query.AddOrderByDescendingClause(orderByDes);
            if (isSortEnabled)
            {
                query = isSortDes ? query.AddOrderByDescendingClause(orderByDes) : query.AddOrderByClause(orderByDes);
            }

            return query;
        }

    }
}
