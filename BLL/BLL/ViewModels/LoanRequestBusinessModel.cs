﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.ViewModels
{
    public class LoanRequestBusinessModel
    {
        public int LoanRequestID { get; set; }
        public string LoanRequeteeName { get; set; }
        public string LoanRequesteeContactNo { get; set; }
        public string LoanRequesteeEmail { get; set; }
        public int LoanRequestStatus { get; set; }
        public DateTime LoanRequestDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool IsPaid { get; set; }
        public int LoanRequestAmount { get; set; }
        public int FkLoanServiceId { get; set; }
        public int FkUserId { get; set; }
        [Display(Name = "Upload Image")]
        public IFormFile[] UploadedImage { get; set; }

        public string AdditionalComments { get; set; }
        public LoanServiceBusinessModel LoanService { get; set; }
        public UserDetailBusinessModel User { get; set; }
        public List<LoanRepaymentBusinessModel> LoanRepaymentBusinessModels { get; set; }


    }
}
