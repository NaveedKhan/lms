﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace BLL.ViewModels
{

    public class FileUploadViewModel
    {
        [DataType(DataType.Upload)]
        public IFormFile File { get; set; }
        public string source { get; set; }
        public string FileName { get; set; }
        public string Base64String { get; set; }
        public long Size { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Extension { get; set; }
        public string FileType { get; set; }
        public string ID { get; set; }
    }
}
