﻿using BOL.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.ViewModels.Identity
{
    public class IdentityResultBusinessModel
    {
        public IdentityResult IdentityResult { get; set; }
        public ApplicationUser User { get; set; }
        public bool Success { get; set; }
    }
}
