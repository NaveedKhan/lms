﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.ViewModels.Identity
{
    public class UserDetailsViewModel
    {
        public int ID { get; set; }
        public int AreaID { get; set; }
        public string[] Days { get; set; }
        public int AreaName { get; set; }
        public int CustomerIdPos { get; set; }
        public string FirstName { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string LastName { get; set; }
        public string RoleName { get; set; }
        public DateTime Joiningdate { get; set; }
        public int RegionID { get; set; }
        public string NewRoleAddedName { get; set; }

        [Display(Name = "Upload Image")]
        public IFormFile UploadedImage { get; set; }

        public bool IsBlackListUser { get; set; }
        public string ImageURL { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }
        public string PlainPassword { get; set; }

        public string DeviceTocken { get; set; }
        public string DeviceType { get; set; }
        public int DeliveryFrequency { get; set; }
        public int BottleSize { get; set; }
        public int BottleSpecialPrice { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string ShortAddress { get; set; }
        public string FormatedAddress { get; set; }
        public string PlaceName { get; set; }
    }
}
