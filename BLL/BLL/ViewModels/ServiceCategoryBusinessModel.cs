﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.ViewModels
{
    public class ServiceCategoryBusinessModel
    {
        public int ServiceCategoryID { get; set; }
        public string ServiceCategoryName { get; set; }
        public bool IsActive { get; set; }
    }
}
