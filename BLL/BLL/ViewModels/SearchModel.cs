﻿
using System;

namespace BLL.ViewModels
{
    public class SearchModel
    {
        public string SearchText { get; set; }
        public string SelectedRole { get; set; }
        public string SelectedDriver { get; set; }
        
        public int SearchIn { get; set; }
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public int SortOrder { get; set; }
        public bool IsSortDes { get; set; } = false;
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }


    }
}
