﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.ViewModels
{
    public class LoanServiceBusinessModel
    {
        
        public int LoanServiceID { get; set; }
        public string LoanServiceName { get; set; }
        public int FkLoanTypeId { get; set; }
        public int FkServiceCategoryId { get; set; }
        public string ImageUrl { get; set; }
        public int MaxLoanAmount { get; set; }
        public string LoanServiceDescription { get; set; }
        public bool IsActive { get; set; }

        [Display(Name = "Upload Image")]
        public IFormFile UploadedImage { get; set; }

        public LoanTypeBusinessModel LoanType { get; set; }
        public ServiceCategoryBusinessModel ServiceCategory { get; set; }
        public List<LoanRequestBusinessModel> LoanRequests { get; set; }
    }
}
