﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.ViewModels
{
    public class DayViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
