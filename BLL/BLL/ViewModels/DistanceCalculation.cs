﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.ViewModels
{

   public class DistanceCalculation
    {
        public double lat { get; set; }
        public double lng { get; set; }
        public int customerId { get; set; }
        public int locationid { get; set; }
        public string address { get; set; }
        public string formatedAddress { get; set; }
        public double distanceFromOffice { get; set; }
        public double distanceFromPreviosStop { get; set; }
        public double distance { get; set; }
        public double duration { get; set; }
        public int order { get; set; }
    }

}
