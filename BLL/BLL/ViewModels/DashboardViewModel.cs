﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.ViewModels
{
    public class DashboardViewModel
    {
        public int TotalCustomers { get; set; }
        public int TotalCustomersAddedThisMonth { get; set; }
        public int TotalAreas { get; set; }
        public int TotalDrivers { get; set; }
        public int TotalNoOfBottlesThisMonth { get; set; }
        public int BottlesInStock { get; set; }
        public int TotalSaleThisMonth { get; set; }
        public int TotalAmountRecieved { get; set; }
        public int Total { get; set; }
        public DriversStatistics DriversStatistics { get; set; }
    }

    public class DriversStatistics 
    {
        public int DriverID { get; set; }
        public int DriverName { get; set; }
        public int TotalBttleSold { get; set; }
        public int TotalCashRecived { get; set; }
    }
}
