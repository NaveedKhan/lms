﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.ViewModels
{
    public class LoanTypeBusinessModel
    {
        public int LoanTypeID { get; set; }
        public string LoanTypeName { get; set; }
        public bool IsActive { get; set; }
    }
}
