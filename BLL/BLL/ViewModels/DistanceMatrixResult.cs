﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.ViewModels
{

    public class DistanceMatrixVM 
    {
        public string DestinationAddresses { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string Duration { get; set; }
        public string Distance { get; set; }
        public string CustomerId { get; set; }
    }
    public class DistanceMatrixResult
    {
        public string[] destination_addresses { get; set; }
        public string[] origin_addresses { get; set; }
        public rows[] rows { get; set; }
        public string status { get; set; }
    }

    public class rows
    {
        public elements[] elements { get; set; }

    }
    public class elements
    {
        public distance distance { get; set; }
        public duration duration { get; set; }
        public string status { get; set; }

    }
    public class distance
    {
        public string text { get; set; }
        public string value { get; set; }

    }
    public class duration
    {
        public string text { get; set; }
        public string value { get; set; }

    }
}
