﻿using BOL.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.ViewModels
{
    public class UserDetailBusinessModel: IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string RoleName { get; set; }
        public bool IsBlackListUser { get; set; }
        public string ImageURL { get; set; }
        public string PlainPassword { get; set; }
        public bool IsDedicatedWorker { get; set; }
        public DateTime Joiningdate { get; set; }

        public string DeviceTocken { get; set; }
        public string DeviceType { get; set; }
        public int DeliveryFrequency { get; set; }
        public int BottleSize { get; set; }
        public int BottleSpecialPrice { get; set; }


        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string ShortAddress { get; set; }
        public string FormatedAddress { get; set; }
        public string PlaceName { get; set; }
        public int CustomerIdPos { get; set; }
    }
}
