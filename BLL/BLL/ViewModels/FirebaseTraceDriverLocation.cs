﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.ViewModels
{
    public class FirebaseTraceDriverLocation
    {
        public string currentdatetime { get; set; }
        public string customerName { get; set; }
        public LocationLatLong destinationPoint { get; set; }
        public string driverID { get; set; }
        public string driverName { get; set; }
        public string id { get; set; }
        public string orderStatus { get; set; }
        public string orderid { get; set; }
        public LocationLatLong[] routeDetail { get; set; }
        public LocationLatLong startingPoint { get; set; }
    }
    public class LocationLatLong
    {
        public double lat { get; set; }
        public double lng { get; set; }
        public DateTime timeStamp {get;set;}
    }
}
