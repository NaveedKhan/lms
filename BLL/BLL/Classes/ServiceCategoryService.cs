﻿using AutoMapper;
using BLL.Interfaces;
using BLL.ViewModels;
using BOL.Models;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Classes
{
    public class ServiceCategoryService : GenericService<ServiceCategory, ServiceCategoryBusinessModel, int>, IServiceCategoryService
    {

        private readonly ISecurityService _securityService;


        public ServiceCategoryService(IMapper mapper,

            ISecurityService securityService, IServiceCategoryRepository serviceCategoryRepository
            ,
            IUnitOfWork unitOfWork
            )
            : base(mapper, serviceCategoryRepository, unitOfWork)
        {
            _securityService = securityService;
        }
    }
}
