﻿using AutoMapper;
using BLL.Interfaces;
using BLL.ViewModels;
using BLL.ViewModels.Identity;
using BOL.Models;
using DAL.Global;
using DAL.Helper;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes
{
    public class UserServices : GenericService<ApplicationUser, UserDetailBusinessModel, int>, IUserServices
    {
        public IConfiguration Configuration { get; }

        private readonly ISecurityService _securityService;


        public UserServices(IMapper mapper,
            IConfiguration configuration,

            ISecurityService securityService, IUsersRepository usersRepository,
            IUnitOfWork unitOfWork
            )
            : base(mapper, usersRepository, unitOfWork)
        {
            Configuration = configuration;
            _securityService = securityService;
        }

        public async Task<PaginatedList<UserDetailBusinessModel>> GetPagedListAsync(int pageNumber, int pageSize, int sortOrder, bool isSortDes, bool isSortClick, string selectedRole)
        {
            var totalCount = await Repository.Get().Where(a => a.RoleName.ToLower() == selectedRole.ToLower()).CountAsync();
            var query = Repository.Get().AsNoTracking().Where(a => a.RoleName.ToLower() == selectedRole.ToLower());
            if (isSortClick)
            {
                query = isSortDes ? query.OrderByDescending(sa => sa.CustomerIdPos) : query.OrderBy(sa => sa.CustomerIdPos);
            }
            var list = PaginatedList<ApplicationUser>.Create(query, pageNumber, pageSize, count);
            var modelsList = Mapper.Map<List<ApplicationUser>, List<UserDetailBusinessModel>>(sortOrder == 0 ? list.OrderByDescending(x => x.Id).ToList() : list.ToList());
            var modelEntity = new PaginatedList<UserDetailBusinessModel>(modelsList, totalCount, pageNumber, pageSize);
            return modelEntity;
        }

        public async Task<PaginatedList<UserDetailBusinessModel>> GetSearchPagedListAsync(SearchModel searchModel)
        {
            string searchText = searchModel.SearchText.Trim();
            PaginatedList<ApplicationUser> list = null;
            Expression<Func<ApplicationUser, bool>> whereCl;
            if (searchModel.SearchIn == 1)
                whereCl = (x => x.RoleName.ToLower() == searchModel.SelectedRole && x.FullName.Contains(searchModel.SearchText));
            else
                whereCl = (x => x.RoleName.ToLower() == searchModel.SelectedRole && x.CustomerIdPos.ToString().Contains(searchModel.SearchText));


            if (searchModel.SortOrder == 0)
                list = GetRecords(searchModel.PageNumber, searchModel.PageSize, ref count, where: whereCl,
                    orderByDes: x => x.Id);
            if (searchModel.SortOrder == 1)
                list = GetRecords(searchModel.PageNumber, searchModel.PageSize, ref count, where: whereCl,
                    orderByDes: x => x.FullName);

            return await PrepareData(searchModel.PageNumber, searchModel.PageSize, count, list);
        }

        private async Task<PaginatedList<UserDetailBusinessModel>> PrepareData(int pageNumber, int pageSize, int totalCount, PaginatedList<ApplicationUser> list)
        {
            var modelsList = Mapper.Map<List<ApplicationUser>, List<UserDetailBusinessModel>>(list);
            var modelEntity = new PaginatedList<UserDetailBusinessModel>(modelsList, totalCount, pageNumber, pageSize);
            //PopulateOriginalSongURL(modelEntity);
            return modelEntity;
        }
        public string Pagination2(int totalRecords, int currentPage, int take = 10, int offset = 6)
        {
            if (totalRecords <= 0)
                return "Not found";

            double rowPerPage = take;
            if (Convert.ToDouble(totalRecords) < take)
                rowPerPage = Convert.ToDouble(totalRecords);

            int totalPage = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(totalRecords) / rowPerPage));
            int pageStart = Convert.ToInt16(Convert.ToDouble(currentPage) - Convert.ToDouble(offset));
            int pageEnd = Convert.ToInt16(Convert.ToDouble(currentPage) + Convert.ToDouble(offset));
            var numPageSb = new StringBuilder();

            if (totalPage < 1)
                return string.Empty;

            numPageSb.Append("<ul class='pagination' id='gridPagination' value='users'>");
            if (currentPage == 1)
                numPageSb.Append("<li class='disabled'><a id='btnPreviousSecond' href=#' disabled>Previous</a></li>");
            else
                numPageSb.Append("<li><a href='javascript:OnPreviousBtnClick(this);' id='btnPreviousSecond' >Previous</a></li>");

            if (currentPage > (offset + 1))
                numPageSb.Append("<li><a href='#' class='navigateToPage'>1</a></li><li class='disabled spacing-dot'><a href='#'>...</a></li>");
            for (int i = 1; i <= totalPage; i++)
            {
                if (pageStart <= i && pageEnd >= i)
                {
                    if (i == currentPage)
                        numPageSb.Append("<li class='active'><a href='#' class='active'>" + i + " <span class='sr-only'>(current)</span></a></li>");
                    else
                        numPageSb.Append("<li class='navigateToPage' id=" + i + "><a href='#'>" + i + "</a></li>");
                }
            }
            if (totalPage > pageEnd)
                numPageSb.Append("<li class='disabled spacing-dot'><a href='#'>...</a></li><li><a href='#' class='navigateToPage'>" + totalPage + "</a></li>");

            if (currentPage == totalPage)
                numPageSb.Append("<li class='disabled'><a id='btnNextSecond' href=#' disabled>Next</a></li>");
            else
                numPageSb.Append("<li><a href='javascript:OnNextBtnClick(this);' id='btnNextSecond'>Next</a></li>");

            numPageSb.Append("</ul>");
            return numPageSb.ToString();
        }
        public string Pagination(int totalRecords, int currentPage, int take = 10, int offset = 6)
        {
            if (totalRecords <= 0)
                return "Not found";

            double rowPerPage = take;
            if (Convert.ToDouble(totalRecords) < take)
                rowPerPage = Convert.ToDouble(totalRecords);

            int totalPage = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(totalRecords) / rowPerPage));
            int pageStart = Convert.ToInt16(Convert.ToDouble(currentPage) - Convert.ToDouble(offset));
            int pageEnd = Convert.ToInt16(Convert.ToDouble(currentPage) + Convert.ToDouble(offset));
            var numPageSb = new StringBuilder();

            if (totalPage < 1)
                return string.Empty;

            numPageSb.Append("<ul class='customPagination'>");
            if (currentPage > (offset + 1))
                numPageSb.Append("<li><a href='#' class='navigateToPage'>1</a></li><li class='disabled spacing-dot'><a href='#'>...</a></li>");
            for (int i = 1; i <= totalPage; i++)
            {
                if (pageStart <= i && pageEnd >= i)
                {
                    if (i == currentPage)
                        numPageSb.Append("<li><a href='#' class='active'>" + i + " <span class='sr-only'>(current)</span></a></li>");
                    else
                        numPageSb.Append("<li class='navigateToPage' id=" + i + "><a href='#'>" + i + "</a></li>");
                }
            }
            if (totalPage > pageEnd)
                numPageSb.Append("<li class='disabled spacing-dot'><a href='#'>...</a></li><li><a href='#' class='navigateToPage'>" + totalPage + "</a></li>");

            numPageSb.Append("</ul>");
            return numPageSb.ToString();
        }

        public async Task<(bool, string)> AddUserDetails(UserDetailsViewModel userInfo)
        {
            bool isSuccess;
            var erroMassage = string.Empty;
            var user = new ApplicationUser
            {
                UserName = userInfo.UserName,
                Email = userInfo.Email,
                FullName = userInfo.FullName,//$"{userInfo.FirstName} {userInfo.LastName}",
                PasswordHash = userInfo.Password,
                RoleName = userInfo.RoleName,
                PhoneNumber = userInfo.PhoneNumber,
                PlainPassword = userInfo.Password,
                Joiningdate = DateTime.Now,
                BottleSize = userInfo.BottleSize,
                DeliveryFrequency = userInfo.DeliveryFrequency,
                BottleSpecialPrice = userInfo.BottleSpecialPrice,
                Address = userInfo.Address,
                CustomerIdPos = userInfo.CustomerIdPos,
                Latitude = userInfo.Latitude,
                Longitude = userInfo.Longitude,
                PlaceName = userInfo.PlaceName,
                ShortAddress = userInfo.ShortAddress,
                FormatedAddress = userInfo.FormatedAddress
            };

            var res = await _securityService.CreateUser(user);

            if (res.Success)
            {
                if (userInfo.UploadedImage != null)
                {
                    await UploadImage(userInfo, res.User);
                    var bmode = Mapper.Map<ApplicationUser, UserDetailBusinessModel>(res.User);
                    AddUpdate(bmode);
                }


                isSuccess = true;
               

                if (UnitOfWork != null)
                {
                    await UnitOfWork.SaveChangesAsync();
                }
            }
            else
            {
                var stringerror = new StringBuilder(" ");
                foreach (var item in res.IdentityResult.Errors)
                {
                    stringerror.Append(item.Code + " : " + item.Description + Environment.NewLine);
                }
                erroMassage = stringerror.ToString();
                isSuccess = false;
            }

            return (isSuccess, erroMassage);
        }

        private async Task UploadImage(UserDetailsViewModel userInfo, ApplicationUser user)
        {
            if (userInfo.UploadedImage != null)
            {

                try
                {
                    string rootPath = "";// Configuration.GetValue<string>("UserData:ImagesStorageRootPath");

                    string UserProfileImageFolder = "";// Configuration.GetValue<string>("UserData:UserProfileImageFolder");
                    string UserProfileImageFullPath = Path.Combine(rootPath, UserProfileImageFolder);
                    var filename = userInfo.UploadedImage.FileName;
                    string userRequestImagesDataPath = Path.Combine(UserProfileImageFullPath, user.Id.ToString() + ".png");
                    if (!System.IO.Directory.Exists(UserProfileImageFullPath))
                    {
                        Directory.CreateDirectory(UserProfileImageFullPath);
                    }
                    string imageStorageRoorURL = "";// Configuration.GetValue<string>("UserData:ImageStorageRoorURL");
                    var imageStorageRoorURLFullURL = Path.Combine(imageStorageRoorURL, UserProfileImageFolder);
                    string webRootUrl = Path.Combine(imageStorageRoorURLFullURL, user.Id.ToString() + ".png");
                    using (var fileStream = new FileStream(userRequestImagesDataPath, FileMode.Create))
                    {
                        await userInfo.UploadedImage.CopyToAsync(fileStream);
                    }
                    user.ImageURL = webRootUrl;
                }
                catch (Exception ex)
                {
                    //ViewData["Errors"] = ex.ToString();
                    //throw;
                }
            }

            if (string.IsNullOrEmpty(user.ImageURL))
            {
                user.ImageURL = "";// Configuration.GetValue<string>("UserData:404ImagePath");
            }
        }

        public async Task<UserDetailBusinessModel> GetUserWithChildTablesById(int id)
        {
            var includeProperties = new Expression<Func<ApplicationUser, object>>[] { };
            Expression<Func<ApplicationUser, bool>> whereCl;
            whereCl = (x => x.Id == id);
            var iis = await GetWithChildTables(whereCl, includeProperties);

            return iis;
        }
    }
}
