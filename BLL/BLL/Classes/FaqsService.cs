﻿using AutoMapper;
using BLL.Interfaces;
using BLL.ViewModels;
using BOL.Models;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes
{
    public class FaqsService : GenericService<FAQs, FAQsBusinessModel, int>, IFaqsService
    {

        private readonly ISecurityService _securityService;
        public FaqsService(IMapper mapper,

            ISecurityService securityService, IFaqsRepository faqsRepository
            ,
            IUnitOfWork unitOfWork
            )
            : base(mapper, faqsRepository, unitOfWork)
        {
            _securityService = securityService;
        }

        public async Task DeleteRecord(int id)
        {

            await Repository.DeleteAsync(x => x.FAQsId == id);
            if (UnitOfWork != null)
            {
                await UnitOfWork.SaveChangesAsync();
            }
        }

    }
}
