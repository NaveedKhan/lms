﻿using AutoMapper;
using BLL.Interfaces;
using BLL.ViewModels;
using BOL.Models;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
namespace BLL.Classes
{
    public class LoanServiceService : GenericService<LoanService, LoanServiceBusinessModel, int>, ILoanServiceService
    {

        private readonly ISecurityService _securityService;
        public LoanServiceService(IMapper mapper,

            ISecurityService securityService, ILoanServiceRepository loanServiceRepository
            ,
            IUnitOfWork unitOfWork
            )
            : base(mapper, loanServiceRepository, unitOfWork)
        {
            _securityService = securityService;
        }

        public async Task DeleteRecord(int id)
        {
            var model = Repository.Get().AsNoTracking().Where(x => x.LoanServiceID == id).FirstOrDefault();
            var entity = Mapper.Map<LoanService, LoanServiceBusinessModel>(model);
            await Repository.DeleteAsync(x => x.LoanServiceID == id);
            if (UnitOfWork != null)
            {
                await UnitOfWork.SaveChangesAsync();
            }
        }

        public async Task<LoanServiceBusinessModel> GeDataWithChildTablesById(int id)
        {
            var includeProperties = new Expression<Func<LoanService, object>>[] { x => x.LoanType, x => x.ServiceCategory };
            Expression<Func<LoanService, bool>> whereCl;
            whereCl = (x => x.LoanServiceID == id);
            var iis = await GetWithChildTables(whereCl, includeProperties);
            return iis;
        }
    }
}
