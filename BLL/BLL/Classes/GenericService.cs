﻿using AutoMapper;
using BLL.Extensions;
using BLL.Interfaces;
using BLL.ViewModels;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BLL.Classes
{
#pragma warning disable S2436 // Types and methods should not have too many generic parameters
    public class GenericService<TDataModel, TBusinessModel, TKey> : IGenericService<TBusinessModel, TKey> where TBusinessModel : class where TDataModel : class
#pragma warning disable S2436 // Types and methods should not have too many generic parameters
    {
        protected readonly IBaseRepository<TDataModel, TKey> Repository;
        protected readonly IMapper Mapper;
        protected readonly IUnitOfWork UnitOfWork;
        protected int count = 0;

        public GenericService(IMapper mapper, IBaseRepository<TDataModel, TKey> genericRepository, IUnitOfWork unitOfWork)
        {
            Repository = genericRepository;
            Mapper = mapper;
            UnitOfWork = unitOfWork;
        }

        public async Task<List<TBusinessModel>> Get()
        {
            var dataEntities = await Repository.Get().AsNoTracking().ToListAsync();
            var businessEntities = Mapper.Map<List<TDataModel>, List<TBusinessModel>>(dataEntities);
            return businessEntities;
        }

        public async Task<TBusinessModel> Get(TKey id)
        {
            var dataEntity = await Repository.GetAsync(id);
            var businessEntity = Mapper.Map<TDataModel, TBusinessModel>(dataEntity);
            return businessEntity;
        }
        public virtual async Task<TBusinessModel> Add(TBusinessModel businessEntity)
        {
            var dataEntity = Mapper.Map<TBusinessModel, TDataModel>(businessEntity);
            dataEntity = await Repository.AddAsync(dataEntity);
            if (UnitOfWork != null)
            {
                await UnitOfWork.SaveChangesAsync();
            }
            businessEntity = Mapper.Map<TDataModel, TBusinessModel>(dataEntity);
            return businessEntity;
        }

        public virtual async Task UpdateAndSave(TBusinessModel businessEntity)
        {
            var dataEntity = Mapper.Map<TBusinessModel, TDataModel>(businessEntity);
            Repository.AddUpdate(dataEntity);
            if (UnitOfWork != null)
            {
                await UnitOfWork.SaveChangesAsync();
            }
        }

        public virtual void AddUpdate(TBusinessModel businessEntity)
        {
            var dataEntity = Mapper.Map<TBusinessModel, TDataModel>(businessEntity);
            Repository.AddUpdate(dataEntity);
        }

        public virtual void Delete(TBusinessModel businessEntity)
        {
            var dataEntity = Mapper.Map<TBusinessModel, TDataModel>(businessEntity);
            Repository.Delete(dataEntity);
        }
        protected PaginatedList<TDataModel> GetRecords(int pageNumber, int pageSize,
            ref int count, int totalCount = 0, Expression<Func<TDataModel, bool>> where = null,
            Expression<Func<TDataModel, object>> orderByExpr = null, Expression<Func<TDataModel,
                object>> orderByDes = null, bool isSortEnabled = false, bool isSortDes = false,
            params Expression<Func<TDataModel, object>>[] includeProperties)
        {
            var data = Repository.Get()
                .AddFilterAndSortExpression(ref count, where, orderByExpr, orderByDes, isSortEnabled, isSortDes, includeProperties)
                .AsNoTracking();

            return PaginatedList<TDataModel>.Create(data, pageNumber, pageSize, totalCount);
        }

        public async Task Update(List<TBusinessModel> businessEntity)
        {
            var dataEntities = Mapper.Map<List<TBusinessModel>, List<TDataModel>>(businessEntity);
            Repository.Update(dataEntities);
            if (UnitOfWork != null)
            {
                await UnitOfWork.SaveChangesAsync();
            }
        }

        public virtual async Task Add(List<TBusinessModel> businessEntity)
        {
            var dataEntities = Mapper.Map<List<TBusinessModel>, List<TDataModel>>(businessEntity);
            await Repository.AddAsync(dataEntities);
        }

        public async Task<TBusinessModel> GetWithChildTables(Expression<Func<TDataModel, bool>> where, params Expression<Func<TDataModel, object>>[] includeProperties)
        {
            var query = Repository.Get().AsNoTracking();
            if (includeProperties != null)
                query = query.IncludeProperties(includeProperties);
            if (where != null)
            {
                query = query.AddWhereClause(where);
                count = await query.CountAsync();
            }
            var dataEntity = Mapper.Map< List < TDataModel > ,List <TBusinessModel>>(await query.ToListAsync());
            return dataEntity.FirstOrDefault();
            // .AsNoTracking();
        }
    }
}
