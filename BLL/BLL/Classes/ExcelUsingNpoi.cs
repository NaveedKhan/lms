﻿using BOL.Models;
using Newtonsoft.Json;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace BLL.Classes
{

    public class Areas
    {
        public int AreaID { get; set; }
        public string AreaName { get; set; }
        public bool IsActive { get; set; }
        public string AssignedDriverName { get; set; }
        public string NoOfCusotomers { get; set; }
    }
    public static class ExcelUsingNpoi
    {

        public static string ReadExcel()
        {
            DataTable dtTable = new DataTable();
            List<string> rowList = new List<string>();
            ISheet sheet;
            using (var stream = new FileStream(@"C:\Logs\ReverbStatements.xlsx", FileMode.Open))
            {
                stream.Position = 0;
                XSSFWorkbook xssWorkbook = new XSSFWorkbook(stream);
                sheet = xssWorkbook.GetSheetAt(0);
                IRow headerRow = sheet.GetRow(0);
                int cellCount = headerRow.LastCellNum;
                for (int j = 0; j < cellCount; j++)
                {
                    ICell cell = headerRow.GetCell(j);
                    if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
                    {
                        dtTable.Columns.Add(cell.ToString());
                    }
                }
                for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    if (row == null) continue;
                    if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                    for (int j = row.FirstCellNum; j < cellCount; j++)
                    {
                        if (row.GetCell(j) != null)
                        {
                            if (!string.IsNullOrEmpty(row.GetCell(j).ToString()) && !string.IsNullOrWhiteSpace(row.GetCell(j).ToString()))
                            {
                                rowList.Add(row.GetCell(j).ToString());
                            }
                            else
                            {
                                rowList.Add("");
                            }
                        }
                        else
                        {
                            rowList.Add("");
                        }
                    }
                    if (rowList.Count > 0)
                        dtTable.Rows.Add(rowList.ToArray());
                    rowList.Clear();
                }
            }
            return JsonConvert.SerializeObject(dtTable);
        }



        public static void WriteExcel()
        {
            List<Areas> persons = new List<Areas>()
            {
                new Areas() {AreaID=1001, AreaName="ABCD", AssignedDriverName ="City1", NoOfCusotomers="USA"},
                new Areas() {AreaID=1002, AreaName="PQRS", AssignedDriverName ="City2", NoOfCusotomers="INDIA"},
                new Areas() {AreaID=1003, AreaName="XYZZ", AssignedDriverName ="City3", NoOfCusotomers="CHINA"},
                new Areas() {AreaID=1004, AreaName="LMNO", AssignedDriverName ="City4", NoOfCusotomers="UK"},
           };

            // Lets converts our object data to Datatable for a simplified logic.
            // Datatable is most easy way to deal with complex datatypes for easy reading and formatting.

            DataTable table = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(persons), (typeof(DataTable)));
            var memoryStream = new MemoryStream();
            if (File.Exists(@"C:\Logs\TestResult.xlsx"))
                File.Delete(@"C:\Logs\TestResult.xlsx");

            using (var fs = new FileStream(@"C:\Logs\TestResult.xlsx", FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Sheet1");

                List<String> columns = new List<string>();
                IRow row = excelSheet.CreateRow(0);
                int columnIndex = 0;

                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);
                    row.CreateCell(columnIndex).SetCellValue(column.ColumnName);
                    columnIndex++;
                }

                IFont boldFont = workbook.CreateFont();
                boldFont.IsBold = true;
                ICellStyle boldStyle = workbook.CreateCellStyle();
                boldStyle.SetFont(boldFont);

                boldStyle.BorderBottom = BorderStyle.DashDot;
                //boldStyle.FillForegroundColor = ;
                boldStyle.FillPattern = FillPattern.SolidForeground;

                int rowIndex = 1;
                foreach (DataRow dsrow in table.Rows)
                {
                    row = excelSheet.CreateRow(rowIndex);
                    int cellIndex = 0;
                    foreach (String col in columns)
                    {
                        var therow = row.CreateCell(cellIndex);
                        therow.SetCellValue(dsrow[col].ToString());
                        therow.CellStyle = boldStyle;
                        cellIndex++;
                    }

                    rowIndex++;
                }
                workbook.Write(fs);

                //to add picture
                //byte[] data = File.ReadAllBytes("Read-write-excel-npoi.jpg");
                //int pictureIndex = workbook.AddPicture(data, PictureType.JPEG);
                //ICreationHelper helper = workbook.GetCreationHelper();
                //IDrawing drawing = excelSheet.CreateDrawingPatriarch();
                //IClientAnchor anchor = helper.CreateClientAnchor();
                //anchor.Col1 = 5;
                //anchor.Row1 = 5;
                //IPicture picture = drawing.CreatePicture(anchor, pictureIndex);
                //picture.Resize();
            }

           


        }

    }
}
