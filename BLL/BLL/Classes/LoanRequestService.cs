﻿using AutoMapper;
using BLL.Interfaces;
using BLL.ViewModels;
using BOL.Models;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes
{
    public class LoanRequestService : GenericService<LoanRequest, LoanRequestBusinessModel, int>, ILoanRequestService
    {
        public LoanRequestService(IMapper mapper,

            ISecurityService securityService, ILoanRequestRepository loanRequestRepository
            ,
            IUnitOfWork unitOfWork
            )
            : base(mapper, loanRequestRepository, unitOfWork)
        {
        }


        public async Task DeleteRecord(int id)
        {
            var model = Repository.Get().AsNoTracking().Where(x => x.LoanRequestID == id).FirstOrDefault();
            var entity = Mapper.Map<LoanRequest, LoanRequestBusinessModel>(model);
            await Repository.DeleteAsync(x => x.LoanRequestID == id);
            if (UnitOfWork != null)
            {
                await UnitOfWork.SaveChangesAsync();
            }
        }

        public async Task<LoanRequestBusinessModel> GeDataWithChildTablesById(int id)
        {
            var includeProperties = new Expression<Func<LoanRequest, object>>[] { x => x.LoanService, x => x.User };
            Expression<Func<LoanRequest, bool>> whereCl;
            whereCl = (x => x.LoanRequestID == id);
            var iis = await GetWithChildTables(whereCl, includeProperties);
            return iis;
        }
    }
}
