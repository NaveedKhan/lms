﻿using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.ViewModels.Identity;
using BOL.Models;
using Microsoft.AspNetCore.Identity;


namespace BLL.Classes
{
    public class SecurityService : ISecurityService
    {
        private readonly SignInManager<ApplicationUser> _signinManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public SecurityService(SignInManager<ApplicationUser> signinManager, UserManager<ApplicationUser> userManager)
        {
            _signinManager = signinManager;
            _userManager = userManager;
        }

        public async Task<bool> SignIn(string email, string password, bool rememberMe)
        {
            var loginResult = await _signinManager.PasswordSignInAsync(email, password, rememberMe, false);
            return loginResult.Succeeded;
        }

        public async Task<bool> RegisterUser(string email, string password)
        {
            var result = await _userManager.CreateAsync(new ApplicationUser { Email = email, UserName = email }, password);
            return result.Succeeded;
        }

        public Task Logout()
        {
            return _signinManager.SignOutAsync();
        }

        public async Task<IdentityResultBusinessModel> CreateUser(ApplicationUser user)
        {
            IdentityResultBusinessModel identityResultmodel = new IdentityResultBusinessModel(); 
            var result = await _userManager.CreateAsync(user, user.PlainPassword);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, user.RoleName);
                identityResultmodel.User = user;
                identityResultmodel.Success = true;
                identityResultmodel.IdentityResult = result;
                return identityResultmodel;
            }
            identityResultmodel.Success = false;
            identityResultmodel.IdentityResult = result;
            return identityResultmodel;
        }
    }
}
