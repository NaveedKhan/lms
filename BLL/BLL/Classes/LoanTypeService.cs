﻿using AutoMapper;
using BLL.Interfaces;
using BLL.ViewModels;
using BOL.Models;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Classes
{
    public class LoanTypeService: GenericService<LoanType, LoanTypeBusinessModel, int>, ILoanTypeService
    {

        private readonly ISecurityService _securityService;
        public LoanTypeService(IMapper mapper,

            ISecurityService securityService, ILoanTypeRepository loanTypeRepository
            ,
            IUnitOfWork unitOfWork
            )
            : base(mapper, loanTypeRepository, unitOfWork)
        {
            _securityService = securityService;
        }
    }
}
