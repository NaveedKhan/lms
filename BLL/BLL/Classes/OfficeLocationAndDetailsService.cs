﻿using AutoMapper;
using BLL.Interfaces;
using BLL.ViewModels;
using BOL.Models;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Classes
{
    public class OfficeLocationAndDetailsService : GenericService<OfficeLocationAndDetails, OfficeLocationAndDetailsBusinessModel, int>, IOfficeLocationAndDetailsService
    {

        public OfficeLocationAndDetailsService(IMapper mapper, IOfficeLocationAndDetailsRepository officeLocationAndDetailsRepository,
            IUnitOfWork unitOfWork
            )
            : base(mapper, officeLocationAndDetailsRepository, unitOfWork)
        {
        }
    }
}
