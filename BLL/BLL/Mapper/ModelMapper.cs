﻿using AutoMapper;
using BLL.Mapper;
using BLL.ViewModels;
using BOL.Models;
using System;
using System.Linq;


namespace BLL.Mapper
{
    public static class ModelMapper
    {
        public static MapperConfiguration Configure()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDetailBusinessModel, ApplicationUser>().ReverseMap();
                cfg.CreateMap<ServiceCategoryBusinessModel, ServiceCategory>().ReverseMap();
                cfg.CreateMap<LoanTypeBusinessModel, LoanType>().ReverseMap();
                cfg.CreateMap<LoanServiceBusinessModel, LoanService>().ReverseMap();
                cfg.CreateMap<LoanRequestBusinessModel, LoanRequest>().ReverseMap();
                cfg.CreateMap<LoanRepaymentBusinessModel, LoanRepayment>().ReverseMap();
                cfg.CreateMap<ContactUsBusinessModel, ContactUs>().ReverseMap();
                cfg.CreateMap<FAQsBusinessModel, FAQs>().ReverseMap();

                CreateMapForInteraPrimitiveTypeConversions(cfg);
            });
        }

        #region Helpers
        private static void CreateMapForInteraPrimitiveTypeConversions(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<string, DateTime>().ConvertUsing<StringToDateTimeConverter>();
        }

        #endregion
    }
}
