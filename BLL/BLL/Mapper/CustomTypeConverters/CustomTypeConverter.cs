﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace BLL.Mapper
{
    internal class StringToDateTimeConverter : ITypeConverter<string, DateTime>
    {
        public DateTime Convert(string source, DateTime destination, ResolutionContext context)
        {
            string formate = "yyyy-MM-dd";
            if (!DateTime.TryParseExact(source, formate, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime))
                return default;

            return dateTime;
        }
    }

    internal class SplitGeners : ITypeConverter<List<string>, List<string>>
    {
        public List<string> Convert(List<string> source, List<string> destination, ResolutionContext context)
        {
            source.ForEach(g =>
            {
                var gs = g.Split("|").ToList();
                if (gs.Any())
                {
                    destination.AddRange(gs);
                }
                else
                    destination.Add(g);
            });

            return destination;
        }
    }
}
