﻿using BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ILoanServiceService : IGenericService<LoanServiceBusinessModel, int>
    {
        Task<LoanServiceBusinessModel> GeDataWithChildTablesById(int id);
        Task DeleteRecord(int id);
    }
}
