﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IGenericService<TBusinessModel, TKey>
    {
        Task<List<TBusinessModel>> Get();
        Task<TBusinessModel> Get(TKey id);
        Task<TBusinessModel> Add(TBusinessModel businessEntity);
        Task Add(List<TBusinessModel> businessEntity);
        Task UpdateAndSave(TBusinessModel businessEntity);
        void AddUpdate(TBusinessModel businessEntity);
        Task Update(List<TBusinessModel> businessEntity);
        void Delete(TBusinessModel businessEntity);
    }
}
