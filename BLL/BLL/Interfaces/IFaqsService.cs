﻿using BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IFaqsService : IGenericService<FAQsBusinessModel, int>
    {
        Task DeleteRecord(int id);
    }
}
