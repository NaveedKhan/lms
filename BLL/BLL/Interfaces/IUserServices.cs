﻿using BLL.ViewModels;
using BLL.ViewModels.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserServices : IGenericService<UserDetailBusinessModel, int>
    {
        Task<PaginatedList<UserDetailBusinessModel>> GetPagedListAsync(int pageNumber = 1, int pageSize = 10, int sortOrder = 0, bool isSortDes = false, bool isSortClick = false, string selectedRole = "DeliveryCustomer");
        Task<PaginatedList<UserDetailBusinessModel>> GetSearchPagedListAsync(SearchModel searchModel);
        Task<UserDetailBusinessModel> GetUserWithChildTablesById(int id);
        Task<(bool, string)> AddUserDetails(UserDetailsViewModel user);
        string Pagination(int totalRecords, int currentPage, int take = 10, int offset = 6);
        string Pagination2(int totalRecords, int currentPage, int take = 10, int offset = 6);
    }
}
