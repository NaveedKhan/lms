﻿using BLL.ViewModels;
using BOL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IDistanceCalculationService
    {
        Task<List<DistanceCalculation>> SortByDistance(DistanceCalculation previousStop, List<DistanceCalculation> locationLists);
        Task <List<DistanceCalculation>> GetSortedLocationList(List<CustomerLocationBusinessModel> locationLists);
        Task<DistanceCalculation> FindNearestCustomerForNewlyAddedCustomer(List<CustomerLocationBusinessModel> locationLists, CustomerLocationBusinessModel newlyAddedLoc);

        // CustomerLocations
    }
}
