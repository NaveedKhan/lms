﻿using BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ILoanRequestService : IGenericService<LoanRequestBusinessModel, int>
    {
        Task<LoanRequestBusinessModel> GeDataWithChildTablesById(int id);
        Task DeleteRecord(int id);
    }
}
