﻿using BLL.ViewModels.Identity;
using BOL.Models;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ISecurityService
    {
        Task<bool> SignIn(string email, string password, bool rememberMe);
        Task<bool> RegisterUser(string email, string password);
        Task<IdentityResultBusinessModel> CreateUser(ApplicationUser user);
        Task Logout();
    }
}
