﻿using BOL.DBContext;
using BOL.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SMAcrCloud.Infrastructure.DependencyResolution.Modules;
using System;
using System.Security.Claims;

namespace Infrastructure
{
    public static class Registration
    {
        public static void RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {
            ConfigurationModule.Configure(services, configuration);
            RepositoryModule.Configure(services, configuration);
            ServiceModule.Configure(services);
        }

        public static void ConfigureDatabaseDefaults(this IApplicationBuilder app)
        {
            RepositoryModule.ConfigureDatabaseDefaults(app);
        }


        public static void ConfigureIdentity(this IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddIdentity<ApplicationUser, IdentityRole<int>>()
            .AddEntityFrameworkStores<AppDBContext>()
            //.AddUserManager<UserManager<ApplicationUser>>()
            //.AddDefaultUI()
            .AddDefaultTokenProviders();



            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.Cookie.Name = "LMS";
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(2);
                options.LoginPath = "/Admin/Login/";
                // ReturnUrlParameter requires 
                //using Microsoft.AspNetCore.Authentication.Cookies;
                options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                options.SlidingExpiration = true;
            });

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                //options.Password.RequiredLength = 6;
                //options.Password.RequiredUniqueChars = 1;

                options.ClaimsIdentity.UserIdClaimType = ClaimTypes.NameIdentifier;
                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;
            });
           

        }
    }
}
