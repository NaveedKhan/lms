﻿using System.Linq;
using DAL.Repository;
using DAL.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using BOL.DBContext;

namespace SMAcrCloud.Infrastructure.DependencyResolution.Modules
{
    public static class RepositoryModule
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDBContext>(options =>
               options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
               b => b.MigrationsAssembly("BOL")
               ));

            services.AddScoped<IRepositoryContext, AppDBContext>();
            services.AddScoped<IUsersRepository, UserRepository>();
            services.AddScoped<ILoanTypeRepository, LoanTypeRepository>();
            services.AddScoped<ILoanServiceRepository, LoanServiceRepository>();
            services.AddScoped<IServiceCategoryRepository, ServiceCategoryRepository>();
            services.AddScoped<IFaqsRepository, FaqsRepository>();
            services.AddScoped<ILoanRequestRepository, LoanRequestRepository>();

            services.AddScoped<IUnitOfWork>(unitOfWork => new UnitOfWork(unitOfWork.GetService<IRepositoryContext>()));
        }
        public static void ConfigureDatabaseDefaults(IApplicationBuilder app)
        {
            using var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<AppDBContext>();
            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }
            context.SaveChanges();
        }
    }
}
