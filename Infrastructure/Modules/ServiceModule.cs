﻿using BLL.Classes;
using BLL.Interfaces;
using BLL.Mapper;
using BOL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace SMAcrCloud.Infrastructure.DependencyResolution.Modules
{
    public static class ServiceModule
    {
        public static void Configure(IServiceCollection services)
        {
            // register auto mapper
            var config = ModelMapper.Configure();
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);



            services.AddScoped<ISecurityService, SecurityService>();
            services.AddScoped<IUserServices, UserServices>();
            services.AddScoped<ILoanTypeService, LoanTypeService>();
            services.AddScoped<IServiceCategoryService, ServiceCategoryService>();
            services.AddScoped<ILoanServiceService, LoanServiceService>();
            services.AddScoped<IFaqsService, FaqsService>();
            services.AddScoped<ILoanRequestService, LoanRequestService>();
        }
    }
}
