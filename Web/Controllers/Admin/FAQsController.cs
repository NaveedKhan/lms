﻿using BLL.Interfaces;
using BLL.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS_Web.Controllers
{


    public class FAQsController : Controller
    {
        private readonly IFaqsService _faqsService;

        public FAQsController(IFaqsService faqsService)
        {
            _faqsService = faqsService;
        }
        // GET: FAQsController1
        public async Task<ActionResult> Index()
        {
            var list = await _faqsService.Get();
            return View(list);
        }

        [HttpGet("/{controller}/MostRelavant")]
        public async Task<ActionResult> TopQuestions()
        {
            var list = await _faqsService.Get();
            return View(list);
        }

        // GET: FAQsController1/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: FAQsController1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FAQsController1/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FAQsBusinessModel model)
        {
            try
            {
                await _faqsService.Add(model);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FAQsController1/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var model = await _faqsService.Get(id);
            return View(model);
        }

        // POST: FAQsController1/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, FAQsBusinessModel model)
        {
            try
            {
                await _faqsService.UpdateAndSave(model);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FAQsController1/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            var model = await _faqsService.Get(id);
            return View(model);
        }

        // POST: FAQsController1/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FAQsBusinessModel model)
        {
            try
            {
                await _faqsService.DeleteRecord(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
