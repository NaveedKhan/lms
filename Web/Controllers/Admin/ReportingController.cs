﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.ViewModels;
using BOL.DBContext;
using BOL.Models;
using DAL.Classes;
using DAL.DataTableToLists;
using DAL.DirectDbConnection;
using DAL.Global;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebApi.Helper;

namespace Deosai.Controllers.Admin
{
    public class ReportingController : Controller
    {
        private readonly AppDBContext _context;
        public IConfiguration Configuration { get; }
        public ReportingController(AppDBContext context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }

        public IActionResult Index()
        {
            var curentdate = DateTime.Now;
            var currnetYear = _context.DeliverySchedules.FirstOrDefault(sa => sa.Year == curentdate.Year && sa.Month == curentdate.Month);

            if (currnetYear != null)
            {
                var todayData = _context.DeliveryScheduleDetails.Where(aa => aa.Day == curentdate.Day &&
            aa.Month == curentdate.Month && aa.DeliveryScheduleID == currnetYear.DeliveryScheduleID)
                    .Include(sa => sa.ApplicationUser)
                    .AsEnumerable();
                ViewData["TotalCustomerServed"] = todayData.Count(sa => sa.CustomerID > 0);
                ViewData["TotalBottleSale"] = todayData.Sum(sa => sa.NoOfBottles);
                ViewData["TotalBottleAmount"] = todayData.Sum(sa => sa.TotalAmount);
                ViewData["TotalRecoveredAmount"] = todayData.Sum(sa => sa.PaidAmount);

                return View(todayData);
            }
            return View(new List<DeliveryScheduleDetails>());
        }

        public IActionResult DownloadReport()
        {
            var curentdate = DateTime.Now;
            var currnetYear = _context.DeliverySchedules.FirstOrDefault(sa => sa.Year == curentdate.Year && sa.Month == curentdate.Month);
            var records = new List<DeliveryScheduleDetails>();
            if (currnetYear != null)
            {
                records = _context.DeliveryScheduleDetails.Where(aa => aa.Day == curentdate.Day &&
              aa.Month == curentdate.Month && aa.DeliveryScheduleID == currnetYear.DeliveryScheduleID)
                      .Include(sa => sa.ApplicationUser)
                      .ToList();
            }


            var sb = new StringBuilder();
            sb.AppendLine("Invoice #, Sale Time, Cust.ID, Name, Sale, Paid, Balance, Payment, Items, Qty, Price");
            foreach (var item in records)
            {
                var saletme = item.DeliveredAt.ToShortTimeString();
                sb.AppendLine(string.Format("\"{0}\",{1},\"{2}\",{3},\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\"",
                                "3343-2021",
                                saletme,
                                item.CustomerID.ToString(),
                                item.ApplicationUser.FullName,
                                item.TotalAmount.ToString(),
                                item.PaidAmount.ToString(),
                                (item.TotalAmount - item.PaidAmount).ToString(),
                                "Cash",
                                "19LtrBottle",
                                item.NoOfBottles.ToString(),
                                item.BottlePrice.ToString()
                                ));
            }
            
            var logFilesPath = @"C:\Logs\DeosaiFiles\LogFiles\";
            if (!Directory.Exists(logFilesPath))
                Directory.CreateDirectory(logFilesPath);

            var date = DateTime.Now.ToString("dd.mm.yy hh.mm.ss");
            var filename = string.Format("Daily.Sale_{0}.csv", date);
            var outputpath = Path.Combine(logFilesPath, filename);
          //  outputpath = string.Format(outputpath, date);
            var csvfileiinfo = new FileInfo(outputpath);
            if (csvfileiinfo.Exists)
                csvfileiinfo.Delete();

            ExcelHeper.CreateCSVFile(sb.ToString(), outputpath);
            var xlsFileBytes = FileProcessor.GetBytes(outputpath).Result;
            if (xlsFileBytes != null && xlsFileBytes.Any())
                return File(xlsFileBytes, "application/octet-stream", $"{filename}_{Guid.NewGuid()}.csv");
            else
            {
                return RedirectToAction("Index");
            }
        }

        public IActionResult AllReports()
        {
            var list = new List<DeliveryReportVM>();
            list = GetReportData();
            ViewData["TotalNoOfDeliveries"] = list.Sum(sa => sa.TotalNoOfDeliveries);
            ViewData["TotalBottleDelivered"] = list.Sum(sa => sa.TotalBottleDelivered);
            ViewData["TotalAmount"] = list.Sum(sa => sa.TotalAmount);
            ViewData["TotalRecoverAmount"] = list.Sum(sa => sa.TotalRecoverAmount);
            ViewData["TotalNoOfDeliveredOrders"] = list.Sum(sa => sa.TotalNoOfDeliveredOrders);
            return View(list);
        }

        private List<DeliveryReportVM> GetReportData()
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            var list = new List<DeliveryReportVM>();
            var query = @"select 
                        SUM(dsd.NoOfBottles) as 'TotalBottleDelivered',
                        sum(dsd.TotalAmount) as 'TotalAmount',
                        sum(dsd.PaidAmount)  as 'TotalRecoverAmount',
                        COUNT(dsd.DeliveryScheduleDetailsID) as 'TotalNoOfDeliveries',
                        (select count(*) from DeliveryScheduleDetails where cast(DeliveryScheduleDetails.DeliveryDate as date) = cast(dsd.DeliveryDate as date)
                        and DeliveryScheduleDetails.IsDelivered = 1) as 'TotalNoOfDeliveredOrders',
                        dsd.DeliveryDate as 'DeliveryDate'
                         from   DeliveryScheduleDetails dsd
                       -- where DeliveryDate like '%2021-03-13%'
                         where DeliveryDate < = GETDATE() 
                        group by dsd.DeliveryDate;";
            var resultdata = DirectQueries.ExecuteQuery_DT(query, connectionString);
            return list = ConvertDTToList.ConvertDataTable<DeliveryReportVM>(resultdata);
        }
    }
}
