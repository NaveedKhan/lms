﻿using AutoMapper;
using BLL.Interfaces;
using BLL.ViewModels;
using BOL.DBContext;
using BOL.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LMS_Web.Controllers
{
    public class LoanServiceController : Controller
    {
        private readonly AppDBContext _context;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly ILoanServiceService _loanServiceService;
        IMapper _mapper;
        public LoanServiceController(AppDBContext context, IMapper mapper, IWebHostEnvironment hostingEnvironment, ILoanServiceService loanServiceService)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            _loanServiceService = loanServiceService;
        }
        // GET: LoanServiceController
        public ActionResult Index()
        {
            return View(_context.LoanServices.Include(a => a.LoanType).Include(b => b.ServiceCategory).ToList());
        }

        // GET: LoanServiceController/Details/5
        public ActionResult Details(int id)
        {
            var selectedData = _context.LoanServices.AsNoTracking().Include(a => a.LoanType).Include(b => b.ServiceCategory).FirstOrDefault(sa => sa.LoanServiceID == id);
            return View(selectedData);
        }

        // GET: LoanServiceController/Create
        public ActionResult Create()
        {
            ViewData["FkLoanTypeId"] = new SelectList(_context.LoanTypes, "LoanTypeID", "LoanTypeName");
            ViewData["FkServiceCategoryId"] = new SelectList(_context.ServiceCategories, "ServiceCategoryID", "ServiceCategoryName");
            return View();
        }

        // POST: LoanServiceController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(LoanServiceBusinessModel model)
        {
            try
            {
                await UploadImage(model);
                await _loanServiceService.Add(model);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        private async Task UpdateImage(LoanServiceBusinessModel model)
        {
            if (model.UploadedImage != null)
            {

                try
                {

                    string folderName = "Images/LoanService/";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string loanServiceImagestoragepath = Path.Combine(webRootPath, folderName);
                    if (!string.IsNullOrEmpty(model.ImageUrl))
                    {
                        var oldimagepath = Path.Combine(webRootPath, model.ImageUrl);
                        var file = new FileInfo(oldimagepath);
                        file.Delete();
                    }


                    if (!Directory.Exists(loanServiceImagestoragepath))
                    {
                        Directory.CreateDirectory(loanServiceImagestoragepath);
                    }

                    Regex reg = new Regex("[*'\":$-,_&#^@]");
                    var filename = reg.Replace(model.UploadedImage.FileName, string.Empty).Replace(" ", "");
                    //var filename =.Replace(" ", "");
                    loanServiceImagestoragepath = Path.Combine(loanServiceImagestoragepath, filename);
                    using (var fileStream = new FileStream(loanServiceImagestoragepath, FileMode.Create))
                    {
                        await model.UploadedImage.CopyToAsync(fileStream);
                    }

                    var imageurl = Path.Combine(folderName, filename);
                    model.ImageUrl = imageurl;
                }
                catch (Exception ex)
                {
                    //ViewData["Errors"] = ex.ToString();
                    //throw;
                }
            }

            if (string.IsNullOrEmpty(model.ImageUrl))
            {
                model.ImageUrl = "";// Configuration.GetValue<string>("UserData:404ImagePath");
            }
        }


        private async Task UploadImage(LoanServiceBusinessModel model)
        {
            if (model.UploadedImage != null)
            {

                try
                {
                    string folderName = "Images/LoanService/";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string loanServiceImagestoragepath = Path.Combine(webRootPath, folderName);
                    if (!Directory.Exists(loanServiceImagestoragepath))
                    {
                        Directory.CreateDirectory(loanServiceImagestoragepath);
                    }

                    Regex reg = new Regex("[*'\":$-,_&#^@]");
                    var filename = reg.Replace(model.UploadedImage.FileName, string.Empty).Replace(" ", "");
                    loanServiceImagestoragepath = Path.Combine(loanServiceImagestoragepath, filename);
                    using (var fileStream = new FileStream(loanServiceImagestoragepath, FileMode.Create))
                    {
                        await model.UploadedImage.CopyToAsync(fileStream);
                    }

                    var imageurl = Path.Combine(folderName, filename);
                    model.ImageUrl = imageurl;
                }
                catch (Exception ex)
                {
                    //ViewData["Errors"] = ex.ToString();
                    //throw;
                }
            }

            if (string.IsNullOrEmpty(model.ImageUrl))
            {
                model.ImageUrl = "";// Configuration.GetValue<string>("UserData:404ImagePath");
            }
        }


        // GET: LoanServiceController/Edit/5
        public ActionResult Edit(int id)
        {
            var selectedData = _context.LoanServices.AsNoTracking().Include(a => a.LoanType).Include(b => b.ServiceCategory).FirstOrDefault(sa => sa.LoanServiceID == id);
            var loanserviceBM = _mapper.Map<LoanService, LoanServiceBusinessModel>(selectedData);
            ViewData["FkLoanTypeId"] = new SelectList(_context.LoanTypes, "LoanTypeID", "LoanTypeName", loanserviceBM.FkLoanTypeId);
            ViewData["FkServiceCategoryId"] = new SelectList(_context.ServiceCategories, "ServiceCategoryID", "ServiceCategoryName", loanserviceBM.FkServiceCategoryId);

            return View(loanserviceBM);
        }

        // POST: LoanServiceController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, LoanServiceBusinessModel model)
        {
            try
            {
                await UpdateImage(model);
                await _loanServiceService.UpdateAndSave(model);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View(model);
            }
        }

        // GET: LoanServiceController/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            var dsd = await _loanServiceService.GeDataWithChildTablesById(id);
            return View(dsd);
        }

        // POST: LoanServiceController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, LoanServiceBusinessModel model)
        {
            try
            {
                await _loanServiceService.DeleteRecord(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
