﻿using BLL.Interfaces;
using BLL.ViewModels;
using BOL.DBContext;
using BOL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DAL.Global.Constants;

namespace LMS_Web.Controllers.Admin
{
    public class LoanRequestsController : Controller
    {

        private readonly ILogger<LoanRequestsController> _logger;
        private readonly AppDBContext _context;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _usermanager;
        private readonly ILoanRequestService _loanRequestService;
        public LoanRequestsController(ILogger<LoanRequestsController> logger,
            AppDBContext context, UserManager<ApplicationUser> usermanager,
            ILoanRequestService loanRequestService,
            IWebHostEnvironment hostingEnvironment)
        {
            _logger = logger;
            _context = context;
            _usermanager = usermanager;
            _loanRequestService = loanRequestService;
            _hostingEnvironment = hostingEnvironment;
        }



        // GET: LoanRequestsController
        public async Task<ActionResult> Index()
        {
            var loanHistory = await _context.LoanRequests.Include(s => s.LoanRepayments).Include(sa => sa.LoanService)
                .ThenInclude(sa => sa.LoanType).Include(s => s.LoanService.ServiceCategory)
                .ToListAsync();

            return View(loanHistory);
        }

        [HttpGet("{controller}/LoanRequests/")]
        public async Task<ActionResult> LoanRequests()
        {
            var loanHistory = await _context.LoanRequests.Include(s => s.LoanRepayments).Include(sa => sa.LoanService)
                .ThenInclude(sa => sa.LoanType).Include(s => s.LoanService.ServiceCategory)
                .Where(r => r.LoanRequestStatus == (int)LoanRequestStatus.Pending)
                .ToListAsync();

            return View(loanHistory);
        }

        public ActionResult ApproveLoan(int id)
        {
            var request = _context.LoanRequests.FirstOrDefault(s => s.LoanRequestID == id);
            if (request != null)
            {
                request.LoanRequestStatus = (int)LoanRequestStatus.Approved;
                request.PaymentDate = DateTime.Now;
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult DeclineLoan(int id)
        {
            var request = _context.LoanRequests.FirstOrDefault(s => s.LoanRequestID == id);
            if (request != null)
            {
                request.LoanRequestStatus = (int)LoanRequestStatus.Rejected;
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpGet("{controller}/LoanHistory/{id}")]
        public async Task<ActionResult> LoanHistory(int id = 0)
        {
            var userid = id;
            if (id == 0)
            {
                var user = await _usermanager.GetUserAsync(User);
                if (user != null)
                    userid = user.Id;
            }

            var loanHistory = await _context.LoanRequests.Include(s => s.LoanRepayments).Include(sa => sa.LoanService)
                .ThenInclude(sa => sa.LoanType).Include(s => s.LoanService.ServiceCategory)
                .Where(r => r.FkUserId == userid && r.LoanRequestStatus != (int)LoanRequestStatus.Pending)
                .ToListAsync();

            return View(loanHistory);
        }

        [HttpGet("{controller}/PendingLoans/{id}")]
        public async Task<ActionResult> PendingLoans(int id = 0)
        {
            var userid = id;
            if (id == 0)
            {
                var user = await _usermanager.GetUserAsync(User);
                if (user != null)
                    userid = user.Id;
            }

            var loanHistory = await _context.LoanRequests.Include(s => s.LoanRepayments).Include(sa => sa.LoanService)
                .ThenInclude(sa => sa.LoanType).Include(s => s.LoanService.ServiceCategory)
                .Where(r => r.FkUserId == userid && r.LoanRequestStatus != (int)LoanRequestStatus.Rejected)
                .ToListAsync();

            return View(loanHistory);
        }

        public ActionResult LoanRepayments(int id)
        {
            return View(new LoanRepayment { FkLoanRequestID = id });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoanRepayments(LoanRepayment model)
        {
            if (!ModelState.IsValid)
            {
                var stringerror = new StringBuilder(" ");
                foreach (var item in ModelState)
                {
                    if (item.Value.Errors.Count > 0)
                        stringerror.Append(item.Value.Errors[0].ErrorMessage + " : " + item.Key + Environment.NewLine);
                }
                ViewData["Errors"] = stringerror.ToString();
                ViewBag.Error = stringerror.ToString();
                return View(model);
            }
            model.PaymentDate = DateTime.Now;
            _context.Add(model);
            var request = _context.LoanRequests.FirstOrDefault(s => s.LoanRequestID == model.FkLoanRequestID);
            if (request != null)
            {
                request.IsPaid = true;
            }
            _context.SaveChanges();
            ViewBag.Message = "Your Loan Is Successfully Paid!";
            return View(model);
        }
        // GET: LoanRequestsController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LoanRequestsController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LoanRequestsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LoanRequestsController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LoanRequestsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, LoanRequestBusinessModel model)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LoanRequestsController/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            var dsd = await _loanRequestService.GeDataWithChildTablesById(id);
            return View(dsd);
        }

        // POST: LoanRequestsController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, LoanRequestBusinessModel model)
        {
            try
            {
                await _loanRequestService.DeleteRecord(id);
                return RedirectToAction(nameof(PendingLoans), new { id = model.FkUserId });
            }
            catch
            {
                return View();
            }
        }
    }
}
