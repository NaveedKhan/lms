﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BOL.DBContext;
using BOL.Models;
using Deosai.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Text;

using BLL.ViewModels;
using WebApi.Helper;
using System.Data;
using DAL.Global;
using static DAL.Global.Constants;
using BLL.Interfaces;
using BLL.ViewModels.Identity;
using Microsoft.AspNetCore.Hosting;

namespace Deosai.Controllers.Admin
{
    public class UsersController : Controller
    {
        private readonly AppDBContext _context;
        private static int  pagenO = 1;
        private readonly UserManager<ApplicationUser> _usermanager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IUserServices _userServices;

        public IConfiguration Configuration { get; }

        public UsersController(AppDBContext context, 
            IUserServices userServices, UserManager<ApplicationUser> usermanager,
            SignInManager<ApplicationUser> signInManager, IConfiguration configuration)
        {
            _context = context;
            _userServices = userServices;
            _usermanager = usermanager;
            _signInManager = signInManager;
            Configuration = configuration;
        }

        // GET: Users
        public async Task<IActionResult> Index(int sortOrder, bool isSortDes = false, bool isSortClick = false, int pageNumber = 1, int pageSize = 10, bool isAjax = false, string selectedRole = "User")
        {
            pagenO  = pageNumber;
            ViewData["RoleName"] = new SelectList(_context.Roles.Where(aa => aa.Name != "Admin"), "Name", "Name", selectedRole);
            var list = await _userServices.GetPagedListAsync(pageNumber, pageSize, sortOrder, isSortDes, isSortClick, selectedRole);
            string paging = _userServices.Pagination2(list.TotalRecords, pageNumber);
            ViewBag.Paging = paging;
            return isAjax ? WithoutSearchResponse(list) :
                View(list);
        }

        private IActionResult WithoutSearchResponse(PaginatedList<UserDetailBusinessModel> result)
        {
            string paging = _userServices.Pagination2(result.TotalRecords, result.PageIndex);
            return new JsonResult(
                new
                {
                    data = result,
                    totalPages = result.TotalPages,
                    isNextBtnAvailable = result.HasNextPage,
                    isPreviousBtnAvailable = result.HasPreviousPage,
                    isUsersResult = true,
                    pagingHtml = paging
                });
        }


        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _userServices.Get(id ?? 0);


            if (applicationUser == null)
            {
                return NotFound();
            }

            string markers = "[";
            markers += "];";
            ViewBag.Markers = markers;


            return View(applicationUser);
        }


        public async Task<IActionResult> Search(SearchModel searchModel)
        {
            if (string.IsNullOrEmpty(searchModel.SearchText))
                return NotFound();
            return await SearchResponse(searchModel);

        }
        public async Task<IActionResult> SearchResponse(SearchModel searchModel)
        {
            var result = await _userServices.GetSearchPagedListAsync(searchModel);
            string paging = _userServices.Pagination2(result.TotalRecords, searchModel.PageNumber);
            return new JsonResult(new
            {
                data = result,
                totalPages = result.TotalPages,
                isNextBtnAvailable = result.HasNextPage,
                isPreviousBtnAvailable = result.HasPreviousPage,
                isUsersResult = true,
                pagingHtml = paging
            });
        }


        // GET: Users/Create
        public IActionResult Create()
        {
            var aa = _context.Roles.AsEnumerable();
            ViewData["RoleName"] = new SelectList(_context.Roles.Where(aa => aa.Name != "Admin"), "Name", "Name");
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(UserDetailsViewModel userInfo)
        {
            ApplicationUser user = null;
            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                {
                    var stringerror = new StringBuilder(" ");
                    foreach (var item in ModelState)
                    {
                        if (item.Value.Errors.Count > 0)
                            stringerror.Append(item.Value.Errors[0].ErrorMessage + " : " + item.Key + Environment.NewLine);
                    }
                    ViewData["Errors"] = stringerror.ToString();
                    ViewData["RoleName"] = new SelectList(_context.Roles, "Name", "Name");
                    return View(userInfo);
                }

                userInfo.Email = $"{HelperMethods.GenerateRandomString()}@gmail.com";
                userInfo.UserName = userInfo.FullName.RemoveSpecialCharactersUsingRegex().Replace(" ", "") + HelperMethods.GenerateRandomString().Substring(4).RemoveSpecialCharactersUsingRegex();
                var (success, erromessage) = await _userServices.AddUserDetails(userInfo);
                //  var result = await CreateIdentityUser(new UserDetailViewModel());

                if (success)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    ViewData["Errors"] = erromessage;
                }
                ViewData["RoleName"] = new SelectList(_context.Roles, "Name", "Name");
                return View(userInfo);
            }
            catch (Exception ex)
            {
                ViewData["RoleName"] = new SelectList(_context.Roles, "Name", "Name");
                ViewData["Errors"] = ex.Message;
                if (user != null)
                {
                    _context.Users.Remove(user);
                    _context.SaveChanges();
                }
                return View(userInfo);
            }
        }


        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users.FindAsync(id);
            if (applicationUser == null)
            {
                return NotFound();
            }


            string markers = "[";
            markers += "];";
            ViewBag.Markers = markers;
            return View(applicationUser);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ApplicationUser model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
                    if (user == null)
                        return NotFound();

                    user.FullName = model.FullName;
                    user.Address = model.Address;
                    user.Joiningdate = model.Joiningdate;

                    user.BottleSpecialPrice = model.BottleSpecialPrice;
                    user.Email = model.Email;
                    user.PhoneNumber = model.PhoneNumber;
                    user.CustomerIdPos = model.CustomerIdPos;
                    await _context.SaveChangesAsync();



                    //_context.Update(user);

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationUserExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index),new { pageNumber =pagenO });
            }
            return View(model);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var applicationUser = await _context.Users.FindAsync(id);
            _context.Users.Remove(applicationUser);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { pageNumber = pagenO });
        }

        private bool ApplicationUserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }


        #region Private Methods


        [HttpPost]
        // [ValidateAntiForgeryToken]
        public async Task<object> ChangePassword(string usernme, string newpassword)
        {
            if (usernme == null || newpassword == null)
                return new { success = false, message = "Incomplete data !", data = new { } };


            var user = await _usermanager.FindByNameAsync(usernme);
            if (user == null)
            {
                return new { success = false, message = "User is deleted or not available !", data = new { } };
            }
            var code = await _usermanager.GeneratePasswordResetTokenAsync(user);
            //HttpContext.Session.Set<UserInfo>("user", model);
            var result = await _usermanager.ResetPasswordAsync(user, code, newpassword);
            if (result.Succeeded)
            {
                user.PlainPassword = newpassword;
                _context.SaveChanges();
                return new { success = true, message = "password changed successfully !", data = new { } };
            }
            var stringerror = new StringBuilder(" ");
            foreach (var item in result.Errors)
            {
                stringerror.Append(item.Code + " : " + item.Description + Environment.NewLine);
            }
            return new { success = false, message = $"Role additon failed reason {stringerror.ToString()}", data = new { } };

        }


 

        #endregion
    }
}
