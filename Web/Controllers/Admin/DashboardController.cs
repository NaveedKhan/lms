﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BLL.ViewModels;
using BOL.DBContext;
using BOL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static DAL.Global.Constants;

namespace Deosai.Controllers.Admin
{
    public class DashboardController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AppDBContext _context;

        public DashboardController(UserManager<ApplicationUser> userManager, AppDBContext context)
        {
            _userManager = userManager;
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Index2()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);// will give the user's userId
            var userName = User.FindFirstValue(ClaimTypes.Name); // will give the user's userName

            // For ASP.NET Core <= 3.1
            ApplicationUser applicationUser = await _userManager.GetUserAsync(User);
            string userEmail = applicationUser?.Email;
            var model = new DashboardViewModel(); //DeliveryCustomer
            var totalcust = await _context.Users.CountAsync(a => a.RoleName == "DeliveryCustomer");
            var totalcustThisMonth0 = await _context.Users.CountAsync(a => a.RoleName == "DeliveryCustomer" &&
            a.Joiningdate.Year == DateTime.Now.Year
            && a.Joiningdate.Month == DateTime.Now.Month);
            var totaldrivers = await _context.Users.CountAsync(a => a.RoleName == "Driver");
            //DashboardViewModel
            return View();
        }

        public object GetDashboardData()
        {
            var totalborrowers = _context.Users.Where(a => a.LoanRequests.Any()).Count();
            var totalopenloans = _context.LoanRequests.Where(a => a.LoanRequestStatus == (int)LoanRequestStatus.Pending).Count();
            var totalfullypaidloans = _context.LoanRequests.Where(a => a.IsPaid).Count();
            var totalcollection = _context.LoanRepayments.Sum(a => a.TransactionAmount);
            var totalLoanReleased = _context.LoanRequests.Where(a => !a.IsPaid && a.LoanRequestStatus == (int)LoanRequestStatus.Approved).Count();

            var monthArry = new List<string> { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
            var monthstrings = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                var date = DateTime.Now.AddMonths(-i);
                var datestring = $"{monthArry[date.Month - 1]}_{date.Year}";
                monthstrings.Add(datestring);
            }

            var totalLoanReleasedLoanAmountByMonth = _context.LoanRequests.Where(a => !a.IsPaid && a.LoanRequestStatus == (int)LoanRequestStatus.Approved &&
            a.PaymentDate <= DateTime.Now && a.PaymentDate >= DateTime.Now.AddMonths(-5))
                .Select(a => new LoanCollectionModel
                {
                    MonthName = $"{monthArry[a.PaymentDate.Month - 1]}_{a.PaymentDate.Year}",
                    Amount = a.LoanRequestAmount
                }).ToArray();
            var releasedAmountLaonByMonthGroupData = new List<LoanCollectionModel>();
            foreach (var item in monthstrings)
            {
                releasedAmountLaonByMonthGroupData.Add(new LoanCollectionModel
                {
                    MonthName = item,
                    Amount = totalLoanReleasedLoanAmountByMonth.Where(s => s.MonthName == item).Sum(s => s.Amount)
                });
            }



            var totalLoanCollectLoanAmountByMonth = _context.LoanRepayments.Where(a => a.PaymentDate <= DateTime.Now && a.PaymentDate >= DateTime.Now.AddMonths(-5))
                .Select(a => new LoanCollectionModel
                {
                    MonthName = $"{monthArry[a.PaymentDate.Month - 1]}_{a.PaymentDate.Year}",
                    Amount = a.TransactionAmount
                }).ToArray();

            var collectAmountLaonByMonthGroupData = new List<LoanCollectionModel>();
            foreach (var item in monthstrings)
            {
                collectAmountLaonByMonthGroupData.Add(new LoanCollectionModel
                {
                    MonthName = item,
                    Amount = totalLoanCollectLoanAmountByMonth.Where(s => s.MonthName == item).Sum(s => s.Amount)
                });
            }

            return new
            {
                success = true,
                Data = new
                {
                    TotalNoOFBorrowers = totalborrowers,
                    TotalOpenLoans = totalopenloans,
                    TotalFullyPaidLoans = totalfullypaidloans,
                    TotalCollection = totalcollection,
                    TotalLoanReleased = totalLoanReleased,
                    MonthlyLoanReleasedData = releasedAmountLaonByMonthGroupData,
                    MonthlyLoanCollectionData = collectAmountLaonByMonthGroupData
                }
            };
        }
    }
}

class LoanCollectionModel
{
    public string MonthName { get; set; }
    public int Amount { get; set; }
}