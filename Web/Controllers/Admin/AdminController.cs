﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using BOL.DBContext;
using BOL.Models;
using Deosai.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LMS_Web.Models;
using BLL.Interfaces;

namespace Deosai.Controllers.Admin
{
    public class AdminController : Controller
    {

        private readonly AppDBContext _context;
        private readonly UserManager<ApplicationUser> _usermanager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole<int>> _rolemanager;
        private readonly ILogger<HomeController> _logger;
        private readonly ISecurityService _securityService;


        public AdminController(AppDBContext context, UserManager<ApplicationUser> usermanager, SignInManager<ApplicationUser> signInManager,
            RoleManager<IdentityRole<int>> rolemanager, ILogger<HomeController> logger, ISecurityService securityService)
        {
            _usermanager = usermanager;
            _signInManager = signInManager;
            _rolemanager = rolemanager;
            _context = context;
            _logger = logger;
            _securityService = securityService;
        }


        public IActionResult Index()
        {
            return RedirectToAction("Login");
        }

       

        public  IActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignUp(SignUpViewModel model)
        {
            var user = new ApplicationUser {
                FirstName = model.FirstName, 
                LastName=model.LastName, 
                UserName = model.UserName, 
                Email = model.UserName,
                RoleName = "User",
                PlainPassword = model.Password,
                FullName = $"{model.FirstName} {model.LastName}"
            };
           await _securityService.CreateUser(user);
            return View();
        }

        public async Task<IActionResult> Login()
        {
            return View(new Logins());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(Logins model)
        {
            var message = "Login Failed";
            var user = new ApplicationUser
            {
                Email = model.Email
            };
            try
            {
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, true, false);
                // _signInManager.sign
                if (result.Succeeded)
                {
                    var currentuser = await _context.Users.FirstOrDefaultAsync(aa => aa.UserName == model.UserName);

                    if (currentuser == null)
                    {
                        ViewBag.message = message;
                        return View(model);
                    }

                    var roles = await _usermanager.GetRolesAsync(currentuser);
                    //await _signInManager.SignInAsync(currentuser, true);
                    var isadmin = await _usermanager.IsInRoleAsync(currentuser, "Admin");
                    var isemployee = await _usermanager.IsInRoleAsync(currentuser, "Employee");
                    return RedirectToAction("Index", "Dashboard");
                    //if (isadmin || isemployee)
                    //{
                    //    // return LocalRedirect("~/Users/");
                    //    return RedirectToAction("Index", "Users");
                    //}
                    //else if (roles.Contains("HandyWebUser"))
                    //{
                    //    return Redirect("/Handy_ServiceRequest/");
                    //}
                }
                ViewBag.message = message;
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message + model.UserName + model.Password;
                return View(model);
            }
        }

    }
}
