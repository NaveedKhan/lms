﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.ViewModels;
using BOL.DBContext;
using DAL.Classes;
using DAL.Global;
using Microsoft.AspNetCore.Mvc;
using WebApi.Helper;

namespace Deosai.Controllers.Admin
{
    public class DriverTrackingController : Controller
    {
        private readonly AppDBContext _context;

        public DriverTrackingController(AppDBContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            var driversList = _context.Users.Where(us => us.RoleName == "Driver").ToList();
            return View(driversList);
        }

        public IActionResult TrackDriver(int Id)
        {
            return View();
        }

        [HttpPost("DriverTracking/GetPending")]
        public async Task<IActionResult> DownloadReportFil([FromForm]List<FirebaseTraceDriverLocation> model) 
        {
            var sb = new StringBuilder();
            sb.AppendLine("Order ID, Customer Name, Order Status");
            foreach (var item in model)
            {
                sb.AppendLine(string.Format("\"{0}\",{1},\"{2}\"",
                                 item.orderid,
                                 item.customerName, item.orderStatus
                                 ));
            }

            var logFilesPath = @"C:\Logs\DeosaiFiles\LogFiles\";
            if (!Directory.Exists(logFilesPath))
                Directory.CreateDirectory(logFilesPath);

            var date = DateTime.Now.ToString("dd.mm.yy hh-mm-ss");
            var outputpath = Path.Combine(Path.GetTempPath(), "logs_{0}.csv");
            outputpath = string.Format(outputpath, date);
            var csvfileiinfo = new FileInfo(outputpath);
            if (csvfileiinfo.Exists)
                csvfileiinfo.Delete();

            ExcelHeper.CreateCSVFile(sb.ToString(), outputpath);
            ViewData["Errors"] = "Users SuccessFullyAdded See the result download file for detail";
            var xlsFileBytes = await FileProcessor.GetBytes(outputpath);
            if (xlsFileBytes != null && xlsFileBytes.Any())
                return File(xlsFileBytes, "application/octet-stream","jani.csv");
            else
                return new FileContentResult(xlsFileBytes, "application/octet-stream");
        }

    }
}
