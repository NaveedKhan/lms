﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Deosai.Models;
using DAL.Helper;
using WebApi.Helper;
using Microsoft.AspNetCore.Identity;
using Deosai.Models.ViewModels;
using BOL.Models;
using BOL.DBContext;
using System.Net.Http;
using Newtonsoft.Json;
using BLL.Classes;
using BLL.Interfaces;
using static DAL.Global.Constants;
using LMS_Web.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using BLL.ViewModels;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Text.RegularExpressions;

namespace Deosai.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly AppDBContext _context;
        private readonly UserManager<ApplicationUser> _usermanager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ISecurityService _securityService;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public HomeController(ILogger<HomeController> logger,
            AppDBContext context, UserManager<ApplicationUser> usermanager,
            SignInManager<ApplicationUser> signInManager,
            ISecurityService securityService, IWebHostEnvironment hostingEnvironment)
        {
            _logger = logger;
            _context = context;
            _usermanager = usermanager;
            _signInManager = signInManager;
            _securityService = securityService;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<IActionResult> Index()
        {

            ViewData["LoanServices"] = new SelectList(_context.LoanServices, "LoanServiceID", "LoanServiceName");
            var loanservicesList = await _context.LoanServices.Include(sa => sa.LoanType).Include(sa => sa.ServiceCategory).ToListAsync();
            return View(loanservicesList);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ContactUs(ContactUs model)
        {
            _context.Add(model);
            await _context.SaveChangesAsync();
            ViewBag.Message = "Your query is successfully submitted!";
            return View();
        }
        public async Task<IActionResult> ApplyForLoan(int selectedService = 0)
        {
            var user = await _usermanager.GetUserAsync(User);
            if (user == null)
                return RedirectToAction("SignIn");
            ViewData["FkLoanServiceId"] = new SelectList(_context.LoanServices, "LoanServiceID", "LoanServiceName", selectedService);
            var loanRequestMode = user == null ? new LoanRequestBusinessModel() : new LoanRequestBusinessModel
            {
                FkUserId = user.Id,
                LoanRequeteeName = user.FullName,
                LoanRequesteeEmail = user.Email,
                LoanRequesteeContactNo = user.PhoneNumber,
                FkLoanServiceId = selectedService,
            };
            return View(loanRequestMode);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApplyForLoan(LoanRequestBusinessModel model)
        {
            var user = await _usermanager.GetUserAsync(User);
            if (user != null)
            {
                //model.FkUserId = user.Id;
                var loanrequestDModel = new LoanRequest
                {
                    LoanRequestStatus = (int)LoanRequestStatus.Pending,
                    LoanRequeteeName = model.LoanRequeteeName,
                    AdditionalComments = model.AdditionalComments,
                    LoanRequesteeContactNo = model.LoanRequesteeContactNo,
                    LoanRequesteeEmail = model.LoanRequesteeEmail,
                    FkUserId = user.Id,
                    FkLoanServiceId = model.FkLoanServiceId,
                    LoanRequestAmount = model.LoanRequestAmount,
                    LoanRequestDate = DateTime.Now
                };
                _context.Add(loanrequestDModel);
                await _context.SaveChangesAsync();
                model.LoanRequestID = loanrequestDModel.LoanRequestID;
                await UploadImage(model);
                return RedirectToAction("Index");
            }
            ViewBag.message = "Please Login first and then proceed.";
            return View(model);
        }


        private async Task UploadImage(LoanRequestBusinessModel model)
        {
            if (model.UploadedImage!=null && model.UploadedImage.Length > 0)
            {

                try
                {
                    string folderName = "Images/LoanRequests/" + model.LoanRequestID;
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string loanRequestsImagestoragepath = Path.Combine(webRootPath, folderName);
                    if (!Directory.Exists(loanRequestsImagestoragepath))
                    {
                        Directory.CreateDirectory(loanRequestsImagestoragepath);
                    }
                    foreach (var item in model.UploadedImage)
                    {
                        Regex reg = new Regex("[*'\":$-,_&#^@]");
                        var filename = reg.Replace(item.FileName, string.Empty).Replace(" ", "");
                        var filenamefullpath = Path.Combine(loanRequestsImagestoragepath, filename);
                        using (var fileStream = new FileStream(filenamefullpath, FileMode.Create))
                        {
                            await item.CopyToAsync(fileStream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //ViewData["Errors"] = ex.ToString();
                }
            }
        }




        public async Task<IActionResult> SignOut()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult SignIn(int selectedServiceId = 0)
        {
            return View(new Logins { SelecteServiceId = selectedServiceId });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignIn(Logins model)
        {
            var message = "Login Failed";
            var user = new ApplicationUser
            {
                Email = model.Email
            };
            try
            {
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, true, false);
                // _signInManager.sign
                if (result.Succeeded)
                {
                    var currentuser = await _context.Users.FirstOrDefaultAsync(aa => aa.UserName == model.UserName);
                    if (currentuser == null)
                    {
                        ViewBag.message = message;
                        return View(model);
                    }

                    var roles = await _usermanager.GetRolesAsync(currentuser);
                    //await _signInManager.SignInAsync(currentuser, true);
                    var isadmin = await _usermanager.GetRolesAsync(currentuser);
                    if (isadmin.Contains("User"))
                    {
                        if (model.SelecteServiceId > 0)
                            return RedirectToAction("ApplyForLoan", new { selectedService = model.SelecteServiceId });
                        else
                            return RedirectToAction("Index");
                    }

                    //if (isadmin || isemployee)
                    //{
                    //    // return LocalRedirect("~/Users/");
                    //    return RedirectToAction("Index", "Users");
                    //}
                    //else if (roles.Contains("HandyWebUser"))
                    //{
                    //    return Redirect("/Handy_ServiceRequest/");
                    //}
                }
                ViewBag.message = message;
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message + model.UserName + model.Password;
                return View(model);
            }
        }

        public IActionResult SignUp(int selectedServiceId = 0)
        {
            return View(new SignUpViewModel { SelecteServiceId = selectedServiceId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignUp(SignUpViewModel model)
        {
            var user = new ApplicationUser
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                UserName = model.Email,
                Email = model.Email,
                RoleName = "User",
                PlainPassword = model.Password,
                FullName = $"{model.FirstName} {model.LastName}"
            };
            await _securityService.CreateUser(user);

            var result = await _signInManager.PasswordSignInAsync(user.UserName, user.PlainPassword, true, false);
            if (model.SelecteServiceId > 0)
                return RedirectToAction("ApplyForLoan", new { selectedService = model.SelecteServiceId });
            else
                return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Private Methods

        private async Task<IdentityResult> CreateIdentityUser(ApplicationUser userInfo)
        {

            var user = new ApplicationUser
            {
                UserName = userInfo.UserName,
                Email = userInfo.Email,
                FullName = userInfo.FullName,//$"{userInfo.FirstName} {userInfo.LastName}",
                PasswordHash = userInfo.PlainPassword,
                RoleName = userInfo.RoleName,
                PhoneNumber = userInfo.PhoneNumber,
                PlainPassword = userInfo.PlainPassword,
                Joiningdate = DateTime.Now,
                BottleSize = userInfo.BottleSize,
                DeliveryFrequency = userInfo.DeliveryFrequency,
                BottleSpecialPrice = userInfo.BottleSpecialPrice,
                Address = userInfo.Address
            };
            var result = await _usermanager.CreateAsync(user, userInfo.PlainPassword);

            if (result.Succeeded)
            {
                await _usermanager.AddToRoleAsync(user, user.RoleName);
                return result;
            }

            return result;
        }
        #endregion
    }
}
