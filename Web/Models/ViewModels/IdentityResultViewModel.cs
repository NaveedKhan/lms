﻿using BOL.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Deosai.Models.ViewModels
{
    public class IdentityResultViewModel
    {
        public IdentityResult IdentityResult { get; set; }
        public ApplicationUser User { get; set; }
        public bool Success { get; set; }
    }
}
