﻿using BOL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Deosai.Models.ViewModels
{
   public class DeliveryScheduleVM
    {
        [Key]
        public int DeliveryScheduleID { get; set; }
        public string DeliveryScheduleName { get; set; }
        public bool IsActive { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    
    }
}
