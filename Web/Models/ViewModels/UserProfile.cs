﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Deosai.Models.ViewModels
{
    public class UserProfile
    {
      
            public int ID { get; set; }
            public string Email { get; set; }
            public string UserName { get; set; }
            public string FullName { get; set; }
            public string Rank { get; set; }
            public string NIC { get; set; }
            public string ContactNumber { get; set; }
            public string JoiningDate { get; set; }


            public string Address { get; set; }
            public int CurrentSalary { get; set; }

            public string UserAuthID { get; set; }
          


            public string DepartmentName { get; set; }
     

            public string  UserTypeName { get; set; }
    

        
    }
}
