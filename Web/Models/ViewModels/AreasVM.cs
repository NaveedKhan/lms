﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Deosai.Models.ViewModels
{
   public class AreasVM
    {
        public int AreaID { get; set; }
        public string AreaName { get; set; }
        public bool IsActive { get; set; }
        public string AssignedDriverName { get; set; }
        public int NoOfCusotomers { get; set; }
    }
}
