﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.DirectDbConnection
{
    public static class DirectQueries
    {

        private static readonly string ConnectionString =
                     "Server=DESKTOP-RPLJP4P\\MSSQLSERVER2;Database=KaamTamaam;Trusted_Connection=True;MultipleActiveResultSets=true";

        public static bool Excute_Query_WithoutTransaction(string query)
        {
            int rowscount = 0;
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                using (var command = new SqlCommand(query, conn))
                {
                    command.CommandTimeout = 300;
                    rowscount = command.ExecuteNonQuery();
                }
            }

            return rowscount > 0;
        }

        public static bool Excute_Query(string query)
        {
            int rowscount = 0;
            using (var conn = new SqlConnection(ConnectionString))
            {
                SqlTransaction transaction = null;
                try
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    using (var command = new SqlCommand(query, conn))
                    {
                        command.CommandTimeout = 300;
                        rowscount = command.ExecuteNonQuery();

                        transaction?.Commit();
                    }
                }
                catch (Exception ex)
                {
                    transaction?.Rollback();
                    throw;
                }
            }

            return rowscount > 0;
        }


        public static DataTable ExecuteQuery_DT(string query, string connectionString, string tablename = "")
        {
            DataTable dt = string.IsNullOrEmpty(tablename) ? new DataTable() : new DataTable(tablename);

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (var command = new SqlCommand(query, conn))
                {
                    command.CommandText = query;
                    command.CommandTimeout = 300;
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt);
                    }
                }

            }
            return dt;
        }

        public static DataSet ExecuteQuery_DS(string query, string connectionString, string tableName = "Result")
        {
            var dt = new DataSet();

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (var command = new SqlCommand(query, conn))
                {
                    command.CommandText = query;
                    command.CommandTimeout = 300;
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt, tableName);
                    }
                }
            }

            return dt;
        }



        public static DataSet ExecuteProcedure(string procedureName, string Dbconnection, Dictionary<string, object> parameters)
        {
            var ds = new DataSet();
            using (var connection = new SqlConnection(Dbconnection))
            {

                connection.Open();
                using (var command = new SqlCommand
                {
                    CommandTimeout = 6000,
                    Connection = connection,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = procedureName
                })
                {
                    foreach (var parameter in parameters)
                    {
                        command.Parameters.Add(SetParameter(parameter.Key, parameter.Value, command));
                    }

                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(ds);
                    }
                }
            }

            return ds;
        }

        private static SqlParameter SetParameter(string paramName, object paramValue, SqlCommand command)
        {
            var param10 = command.CreateParameter();
            param10.ParameterName = paramName;
            param10.Value = paramValue;
            return param10;
        }

        public static DataSet SelectQuery(string query)
        {
            try
            {

                var connection = new SqlConnection(ConnectionString);
                // Assumes that connection is a valid SqlConnection object.  
                string queryString = query;
                //"SELECT CustomerID, CompanyName FROM dbo.Customers";
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                DataSet customers = new DataSet();
                adapter.Fill(customers, "Customers");

                return customers;
            }
            catch (System.Exception ex)
            {

                return null;
            }

        }

    }
}
