﻿using BOL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface ILoanServiceRepository : IBaseRepository<LoanService, int>
    {
    }
}
