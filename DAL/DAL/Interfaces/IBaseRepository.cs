﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IBaseRepository<TEntity, TKey>  where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        Task<TEntity> GetById(int id);
        IQueryable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> expression);
        Task Create(TEntity entity);
        Task Update(int id, TEntity entity);
        Task Delete(int id);


        IQueryable<TEntity> Get();

        Task<TEntity> GetAsync(TKey id);
        Task<List<TEntity>> GetAsync(Expression<Func<TEntity, bool>> where);
        Task<List<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");

        Task<TEntity> AddAsync(TEntity entity);
        Task AddAsync(List<TEntity> entities);

        void AddUpdate(TEntity entity);
        void Update(List<TEntity> entities);

        void Delete(TEntity entity);
        void Delete(List<TEntity> entity);
        Task DeleteAsync(Expression<Func<TEntity, bool>> predicate);
        Task DeleteAsync(TKey id);

        void Detach(TEntity entityToDetach);
        void DeleteState(TEntity entityToDelete);

    }
}