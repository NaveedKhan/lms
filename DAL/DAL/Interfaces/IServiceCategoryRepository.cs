﻿using BOL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IServiceCategoryRepository : IBaseRepository<ServiceCategory, int>
    {
    }
}
