﻿using BOL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IFaqsRepository : IBaseRepository<FAQs, int>
    {
    }
}
