﻿using System.Threading.Tasks;
using BOL.DBContext;
using DAL.Interfaces;

namespace DAL.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbContext _dbContext;

        public UnitOfWork(IDbContext context)
        {
            _dbContext = context;
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }

        public async Task ReloadEntities()
        {
            await _dbContext.ReloadEntitiesAsync();
        }
    }
}
