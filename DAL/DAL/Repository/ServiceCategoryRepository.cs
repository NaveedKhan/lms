﻿using BOL.Models;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository
{
    public class ServiceCategoryRepository : BaseRepository<ServiceCategory, int>, IServiceCategoryRepository
    {
        public ServiceCategoryRepository(IRepositoryContext dbContext) : base(dbContext) { }
    }
}
