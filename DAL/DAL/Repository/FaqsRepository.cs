﻿using BOL.Models;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository
{
    public class FaqsRepository : BaseRepository<FAQs, int>, IFaqsRepository
    {
        public FaqsRepository(IRepositoryContext dbContext) : base(dbContext) { }
    }
}
