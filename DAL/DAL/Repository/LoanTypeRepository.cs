﻿using BOL.Models;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository
{
    public class LoanTypeRepository : BaseRepository<LoanType, int>, ILoanTypeRepository
    {
        public LoanTypeRepository(IRepositoryContext dbContext) : base(dbContext) { }
    }
}
