﻿using BOL.Models;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository
{
    public class LoanRequestRepository : BaseRepository<LoanRequest, int>, ILoanRequestRepository
    {
        public LoanRequestRepository(IRepositoryContext dbContext) : base(dbContext) { }
    }
}
