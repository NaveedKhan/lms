﻿using BOL.Models;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository
{
    public class LoanServiceRepository : BaseRepository<LoanService, int>, ILoanServiceRepository
    {
        public LoanServiceRepository(IRepositoryContext dbContext) : base(dbContext) { }
    }
}
