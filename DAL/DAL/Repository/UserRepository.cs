﻿using BOL.Models;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository
{
    public class UserRepository : BaseRepository<ApplicationUser, int>, IUsersRepository
    {
        public UserRepository(IRepositoryContext dbContext) : base(dbContext) { }
    }
}
