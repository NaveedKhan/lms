﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Global
{

    public static class FileProcessor
    {
        public async static Task<byte[]> GetBytes(string filePath)
        {
            var errResp = new byte[0];
            if (string.IsNullOrEmpty(filePath))
                return errResp;
            var fileInfo = new FileInfo(filePath);
            if (!fileInfo.Exists)
                return errResp;
#pragma warning disable S2930 // dispose
            using FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
#pragma warning disable S2930
            byte[] bytes = new byte[fs.Length];
            int numBytesToRead = (int)fs.Length;
            int numBytesRead = 0;
            while (numBytesToRead > 0)
            {
                // Read may return anything from 0 to numBytesToRead.
                int n = await fs.ReadAsync(bytes, numBytesRead, numBytesToRead);

                // Break when the end of the file is reached.
                if (n == 0)
                    break;

                numBytesRead += n;
                numBytesToRead -= n;
            }

            return bytes;
        }
    }
}
