﻿using BOL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Global
{
    public class Constants
    {
        public enum MonthNames
        {
            January,
            Febuary,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }

        public enum LoanRequestStatus
        {
            Pending = 1,
            Approved = 2,
            Rejected = 3
        }

        //var freqString = new List<string> { "daily", "twice", "thrice", "weekly" };
        //var daysString = new List<string> { "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" };

        public enum FrequecyE
        {

            Daily = 1,
            Twice = 3,
            Thrice = 2,
            Weekly = 7
        }

        public enum Days
        {
            Sunday = 0,
            Monday,
            Tuesday,
            Wednesday,
            Thurday,
            Friday,
            Saturday,

        }



    }
}
