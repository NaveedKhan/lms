﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Text;
namespace DAL.Global
{
    /// <summary>
    /// This is a class to handle logging Linq to Sql output to a file.
    /// To use it you can edit the GenericController
    /// </summary>

    public class FileLogger : TextWriter
    {
        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        private string FileName { get; set; } = string.Empty;

        /// <summary>
        /// Writes a character to the text stream.
        /// </summary>
        /// <param name="value">The character to write to the text stream.</param>
        /// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.IO.TextWriter"/> is closed. </exception>
        ///
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        public override void Write(char value)
        {
            Log(value.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Writes a string to the text stream.
        /// </summary>
        /// <param name="value">The string to write.</param>
        /// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.IO.TextWriter"/> is closed. </exception>
        ///
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        public override void Write(string value)
        {
            if (value != null)
            {
                Log(value);
            }
        }

        /// <summary>
        /// Writes a subarray of characters to the text stream.
        /// </summary>
        /// <param name="buffer">The character array to write data from.</param>
        /// <param name="index">Starting index in the buffer.</param>
        /// <param name="count">The number of characters to write.</param>
        /// <exception cref="T:System.ArgumentException">The buffer length minus <paramref name="index"/> is less than <paramref name="count"/>. </exception>
        ///
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="buffer"/> parameter is null. </exception>
        ///
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///   <paramref name="index"/> or <paramref name="count"/> is negative. </exception>
        ///
        /// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.IO.TextWriter"/> is closed. </exception>
        ///
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        public override void Write(char[] buffer, int index, int count)
        {
            if (index < 0 || count < 0 || buffer.Length - index < count)
            {
                base.Write(buffer, index, count);
            }
            Log(new string(buffer, index, count));
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void Log(string message)
        {
            if (string.IsNullOrEmpty(FileName))
                throw new ArgumentException("You must set the FileName property before using the class");

            try
            {
                StreamWriter sw = !File.Exists(FileName) ? File.CreateText(FileName) : File.AppendText(FileName);

                sw.WriteLine(message);
                sw.Close();
            }
            catch (Exception)
            {
                //We don't want the logging screwing up the app, so swallow the exception
            }
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logFileKey">The log file key.</param>
        public static void Log(string message, string logFileKey)
        {
            StreamWriter sw = null;
            try
            {
                var date = DateTime.Now.ToString("dd-MM-yyyy");
                string fileName = logFileKey;//ConfigurationManager.AppSettings[logFileKey];

               
                if (!string.IsNullOrEmpty(fileName))
                {
                    fileName = string.Format(fileName, date);
                    if (!Directory.Exists(Path.GetDirectoryName(fileName)))
                        Directory.CreateDirectory(Path.GetDirectoryName(fileName));
                    sw = !File.Exists(fileName) ? File.CreateText(fileName) : File.AppendText(fileName);

                    sw.WriteLine(message);
                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("process cannot access the file "))
                {

                } 
            }
            finally
            {
                sw?.Close();
            }
        }

        public static void LogsForTesting(string message, string logFileKey)
        {
            StreamWriter sw = null;
            try
            {
                var date = DateTime.Now.ToString("dd-MM-yyyy");
                string fileName = logFileKey;
                if (!string.IsNullOrEmpty(fileName))
                {
                    fileName = string.Format(fileName, date);

                    if (!Directory.Exists(Path.GetDirectoryName(fileName)))
                        Directory.CreateDirectory(Path.GetDirectoryName(fileName));
                    sw = !File.Exists(fileName) ? File.CreateText(fileName) : File.AppendText(fileName);

                    sw.WriteLine(message);
                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("process cannot access the file "))
                {
                  
                }
                   
            }
            finally
            {
                sw?.Close();
            }
        }

        /// <summary>
        /// When overridden in a derived class, returns the <see cref="T:System.Text.Encoding"/> in which the output is written.
        /// </summary>
        /// <returns>The Encoding in which the output is written.</returns>
        public override Encoding Encoding
        {
            get
            {
                return new UnicodeEncoding(false, false);
            }
        }

        public static string ExcelFilePath { get { return "ExcelFilePath"; } }
        public static string ExcelFilePathArtist { get { return "ExcelFilePath_Artist"; } }
        public static string BaseSchedularLogFile { get { return "BaseSchedularLogFile"; } }
        public static string ArtistRegistrationLogFile { get { return "ArtistRegistrationLogFile"; } }
        public static string UpdateGigLogFile { get { return "UpdateGigLogFile"; } }
        public static string VenueVerificationLogs { get { return "VenueVerificationLogs"; } }
        public static string ServiceLog { get { return "ServiceLog"; } }
        public static string CreateStatementServiceLog { get { return "CreateStatementServiceLog"; } }
        public static string StatementProcessLog { get { return "StatementProcessLog"; } }
        public static string UnclaimedFileProcessLog { get { return "UnclaimedFileProcessLog"; } }
        

        public static string StatementLog { get { return "StatementLog"; } }
        public static string UnclaimedRoyaltiesFiles { get { return "UnclaimedRoyaltiesFiles"; } }
        public static string BandsInTownLogs { get { return "BandsInTownLogs"; } }
        public static string LogFolder { get { return "LogFolder"; } }

        public static string LogClaimFiles { get { return "LogClaimFiles"; } }
        public static string TracingPath { get { return "TracingPath"; } }

        
    }
}