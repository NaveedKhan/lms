﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Helper
{
    public class PushNotifications
    {

        public async Task SendNotification(List<string> deviceIds, string message, dynamic data, string title = "Kaam Tamam", bool isNotification = true)
        {
            var devices = deviceIds;// GetUsersDeviceTokenByIds(usersIds);
            if (devices.Count == 0)
                return;
            await SendFcmWebRequest(PrepareFcmData(devices, message, data, title, isNotification));
        }
        public List<string> GetUsersDeviceTokenByIds(List<string> userIds)
        {
            return new List<string>();
        }
        private async Task SendFcmWebRequest(object data)
        {
            var key = "AAAApmnXMCA:APA91bEEgvogk6SVOeCciLoh9a2Fq1Aw8kJ5tUxDYYz3y8lMQq3MhhzsjAFhccIsHrtBJ269_djyt7cFgGA-9ULyMDRfc38T_Nxk9Tr9Gn7HAHZNZ1qsi6vZ42j87YEl7nXGMYpuSvkw";


            var byteArray = GetByteArray(data);
            var serverApiKey = key;//_fcmOptions.ServerKey;
            var tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverApiKey));
            tRequest.ContentLength = byteArray.Length;
            tRequest.UseDefaultCredentials = true;
            tRequest.PreAuthenticate = true;
            tRequest.Credentials = CredentialCache.DefaultCredentials;

            using (var dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                await tRequest.GetResponseAsync();
            }
        }

        private byte[] GetByteArray(object data)
        {
            var json = JsonConvert.SerializeObject(data);
            var byteArray = Encoding.UTF8.GetBytes(json);
            return byteArray;
        }

        private object PrepareFcmData(List<string> deviceIds, string message, dynamic dataObj, string title, bool isNotification = true)
        {
            object data = new
            {
                registration_ids = deviceIds,
                notification = isNotification ? new
                {
                    title,
                    body = message,
                    sound = "default",
                    priority = "high",
                    content_available = true,
                } : null,
                data = new
                {
                    data = dataObj,
                    body = dataObj,
                    title = "Lister",
                    content_available = true,
                    priority = "high",
                }
            };

            return data;
        }
    }
}
