﻿
using BOL.Models;
using DAL.Global;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace WebApi.Helper
{
    public class ExcelHeper
    {


        public Dictionary<string, List<string>> GetExcelDataCities(List<object> provinces)
        {


            return null;
        }

        public static DataSet ReadExcelFile(string filePath)
        {

            IExcelDataReader reader = null;
            var result = new List<ApplicationUser>();
            // string FilePath = @"C:\Logs\WriterInformationRequired_6813.xlsx";
            // FilePath = @"C:\Logs\DeosaiData\monday customer data.xlsx";
            //Load file into a stream
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

            //Must check file extension to adjust the reader to the excel file type
            if (System.IO.Path.GetExtension(filePath).Equals(".xls"))
            {
                reader = ExcelReaderFactory.CreateBinaryReader(stream);
            }
            else if (System.IO.Path.GetExtension(filePath).Equals(".xlsx"))
            {
                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            }


            if (reader != null)
            {
                var ds = reader.AsDataSet();
                if (stream != null) stream.Dispose();
                return ds;
            }

            if (stream != null) stream.Dispose();
            return null;
        }

        public static List<ApplicationUser> ReadData()
        {

            IExcelDataReader reader = null;
            var result = new List<ApplicationUser>();
            // string FilePath = @"C:\Logs\WriterInformationRequired_6813.xlsx";
            string FilePath = @"C:\Logs\DeosaiData\monday customer data.xlsx";
            //Load file into a stream
            FileStream stream = File.Open(FilePath, FileMode.Open, FileAccess.Read);

            //Must check file extension to adjust the reader to the excel file type
            if (System.IO.Path.GetExtension(FilePath).Equals(".xls"))
            {
                reader = ExcelReaderFactory.CreateBinaryReader(stream);
            }
            else if (System.IO.Path.GetExtension(FilePath).Equals(".xlsx"))
            {
                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            }


            if (reader != null)
            {
                //Fill DataSet]
                var ds = reader.AsDataSet();
                try
                {
                    //Loop through rows for the desired worksheet
                    //In this case I use the table index "0" to pick the first worksheet in the workbook
                    var deliveryfreqArr = new List<int> { 1, 2, 3, 7 };
                    Random rand = new Random();
                    var indexE = 0;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        if (ds.Tables[0].Rows.IndexOf(row) > 0)
                        {
                            ++indexE;
                            string name = row[2].ToString();
                            string bottleprice = row[5].ToString();
                            string address = row[4].ToString();
                            // Generate a random index less than the size of the array.  
                            int index = rand.Next(deliveryfreqArr.Count);
                            var user = new ApplicationUser
                            {
                                Address = address.Replace("#", "No"),
                                BottleSpecialPrice = Convert.ToInt32(bottleprice),
                                FullName = name,
                                RoleName = "DeliveryCustomer",
                                UserName = name.RemoveSpecialCharactersUsingRegex(),
                                PlainPassword = "test@123",
                                PhoneNumber = "03461234567",
                                DeliveryFrequency = deliveryfreqArr[index],
                                Email = $"user{indexE}@gmail.com"
                                //Ameer Hamza = G-10/3
                            };
                            result.Add(user);
                        }

                    }
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }



        public static void CreateCSVFile(string sb, string outputpath)
        {
            try
            {
                var utf8WithoutBom = new System.Text.UTF8Encoding(false);
                var directory = Path.GetDirectoryName(outputpath);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                if (!IsFileLocked(outputpath))
                {
                    File.WriteAllText(outputpath, sb.ToString(), utf8WithoutBom);
                }
            }
            catch (Exception ex)
            {
                ///LogError(ex);
                throw;
            }

        }

        public static bool IsFileLocked(string filePath)
        {
            try
            {
                using (File.Open(filePath, FileMode.Open)) { }
            }
            catch (IOException e)
            {
                var errorCode = Marshal.GetHRForException(e) & ((1 << 16) - 1);

                return errorCode == 32 || errorCode == 33;
            }

            return false;
        }
    }
}
