﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BOL.Migrations
{
    public partial class loanrequests : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LoanRequests",
                columns: table => new
                {
                    LoanRequestID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LoanRequeteeName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LoanRequesteeContactNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LoanRequesteeEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LoanRequestAmount = table.Column<int>(type: "int", nullable: false),
                    FkLoanServiceId = table.Column<int>(type: "int", nullable: false),
                    FkUserId = table.Column<int>(type: "int", nullable: false),
                    AdditionalComments = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanRequests", x => x.LoanRequestID);
                    table.ForeignKey(
                        name: "FK_LoanRequests_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LoanRequests_LoanServices_FkLoanServiceId",
                        column: x => x.FkLoanServiceId,
                        principalTable: "LoanServices",
                        principalColumn: "LoanServiceID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LoanRepayments",
                columns: table => new
                {
                    LoanRepaymentID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FkLoanRequestID = table.Column<int>(type: "int", nullable: false),
                    TransactionAmount = table.Column<int>(type: "int", nullable: false),
                    PaymentMethod = table.Column<int>(type: "int", nullable: false),
                    CardDetails = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanRepayments", x => x.LoanRepaymentID);
                    table.ForeignKey(
                        name: "FK_LoanRepayments_LoanRequests_FkLoanRequestID",
                        column: x => x.FkLoanRequestID,
                        principalTable: "LoanRequests",
                        principalColumn: "LoanRequestID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LoanRepayments_FkLoanRequestID",
                table: "LoanRepayments",
                column: "FkLoanRequestID");

            migrationBuilder.CreateIndex(
                name: "IX_LoanRequests_FkLoanServiceId",
                table: "LoanRequests",
                column: "FkLoanServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanRequests_UserId",
                table: "LoanRequests",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoanRepayments");

            migrationBuilder.DropTable(
                name: "LoanRequests");
        }
    }
}
