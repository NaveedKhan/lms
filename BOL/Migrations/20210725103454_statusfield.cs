﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BOL.Migrations
{
    public partial class statusfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LoanRequestStatus",
                table: "LoanRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LoanRequestStatus",
                table: "LoanRequests");
        }
    }
}
