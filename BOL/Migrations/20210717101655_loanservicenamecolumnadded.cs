﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BOL.Migrations
{
    public partial class loanservicenamecolumnadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LoanServiceName",
                table: "LoanServices",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LoanServiceName",
                table: "LoanServices");
        }
    }
}
