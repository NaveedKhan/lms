﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BOL.Migrations
{
    public partial class fkuserdloan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LoanRequests_AspNetUsers_UserId",
                table: "LoanRequests");

            migrationBuilder.DropIndex(
                name: "IX_LoanRequests_UserId",
                table: "LoanRequests");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "LoanRequests");

            migrationBuilder.CreateIndex(
                name: "IX_LoanRequests_FkUserId",
                table: "LoanRequests",
                column: "FkUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_LoanRequests_AspNetUsers_FkUserId",
                table: "LoanRequests",
                column: "FkUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LoanRequests_AspNetUsers_FkUserId",
                table: "LoanRequests");

            migrationBuilder.DropIndex(
                name: "IX_LoanRequests_FkUserId",
                table: "LoanRequests");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "LoanRequests",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LoanRequests_UserId",
                table: "LoanRequests",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_LoanRequests_AspNetUsers_UserId",
                table: "LoanRequests",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
