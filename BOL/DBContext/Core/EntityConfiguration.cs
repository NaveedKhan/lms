﻿using BOL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BOL.DBContext.Core
{
   

    public class LoanTypeEntityConfiguration : IEntityTypeConfiguration<LoanType>
    {
        public void Configure(EntityTypeBuilder<LoanType> builder)
        {
            builder.HasData(
                new LoanType() { LoanTypeID = 1, LoanTypeName = "Fixed Term Loan", IsActive = true },
                new LoanType() { LoanTypeID = 2, LoanTypeName = "Dynamic Term Loan", IsActive = true },
                new LoanType() { LoanTypeID = 3, LoanTypeName = "Interest Free Loan", IsActive = true }
            );
        }
    }

    public class ServiceCategoryEntityConfiguration : IEntityTypeConfiguration<ServiceCategory>
    {
        public void Configure(EntityTypeBuilder<ServiceCategory> builder)
        {
            builder.HasData(
                new ServiceCategory() { ServiceCategoryID = 1, ServiceCategoryName = "Personal Lending", IsActive = true },
                new ServiceCategory() { ServiceCategoryID = 2, ServiceCategoryName = "Purchasing Financing", IsActive = true },
                new ServiceCategory() { ServiceCategoryID = 3, ServiceCategoryName = "SME Lending", IsActive = true },
                new ServiceCategory() { ServiceCategoryID = 4, ServiceCategoryName = "Retail Mortgages", IsActive = true },
                new ServiceCategory() { ServiceCategoryID = 5, ServiceCategoryName = "Commercial", IsActive = true },
                new ServiceCategory() { ServiceCategoryID = 6, ServiceCategoryName = "Others", IsActive = true }
            );
        }
    }

}
