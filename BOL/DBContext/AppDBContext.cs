﻿using BOL.DBContext.Core;
using BOL.Models;
using DAL.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL.DBContext
{
    public class AppDBContext : IdentityDbContext<ApplicationUser, IdentityRole<int>, int>, IRepositoryContext  // IdentityDbContext<ApplicationUser>
    {

        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //builder.Entity<ServiceCategory>(entity =>
            //{
            //    entity.HasKey(e => e.Id);
            //});
            builder.Seed();

            builder.Entity<LoanService>()
                .HasOne(s => s.LoanType)
                .WithMany(w => w.LoanServices)
                .HasForeignKey(s => s.FkLoanTypeId);

            builder.Entity<LoanService>()
            .HasOne(s => s.ServiceCategory)
            .WithMany(w => w.LoanServices)
            .HasForeignKey(s => s.FkServiceCategoryId);

            builder.Entity<LoanRequest>()
           .HasOne(s => s.LoanService)
           .WithMany(w => w.LoanRequests)
           .HasForeignKey(s => s.FkLoanServiceId);

            builder.Entity<LoanRequest>()
          .HasOne(s => s.User)
          .WithMany(w => w.LoanRequests)
          .HasForeignKey(s => s.FkUserId);




            builder.Entity<LoanRepayment>()
          .HasOne(s => s.LoanRequest)
          .WithMany(w => w.LoanRepayments)
          .HasForeignKey(s => s.FkLoanRequestID);


            // // Shorten key length for Identity 
            // builder.Entity<ApplicationUser>(entity =>
            // {/
            //     entity.Property(m => m.Id).HasMaxLength(127)
            //     entity.Property(m => m.Email).HasMaxLength(127)
            //     entity.Property(m => m.NormalizedEmail).HasMaxLength(127)
            //     entity.Property(m => m.NormalizedUserName).HasMaxLength(127)
            //     entity.Property(m => m.UserName).HasMaxLength(127)
            // })

            // builder.Entity<Fingerprint>()
            //     .Property(f => f.FkAcrCatalogueId)
            //     .HasDefaultValue(1)
        }


        public DbSet<LoanType> LoanTypes { get; set; }
        public DbSet<LoanService> LoanServices { get; set; }
        public DbSet<ServiceCategory> ServiceCategories { get; set; }

        public DbSet<Products> Products { get; set; }
        public DbSet<LoanRequest> LoanRequests { get; set; }
        public DbSet<LoanRepayment> LoanRepayments { get; set; }
        public DbSet<ContactUs> ContactUs { get; set; }
        public DbSet<FAQs> FAQs { get; set; }

        public async Task ReloadEntitiesAsync()
        {
            var entries = base.ChangeTracker.Entries().ToList();
            foreach (var entity in entries)
            {
                await entity.ReloadAsync();
            }
        }

    }


    public static class Dbextension
    {

        public static void EnsureSeed(this AppDBContext db, IServiceProvider services, string notfounfimage, RoleManager<IdentityRole<int>> RoleManager, DataTable dt = null)
        {
            BOL.DataSeeder.Seeddata(db, notfounfimage, RoleManager);
        }
    }

    //this technique is used to seed data int database in core > 1 version using add migration
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new LoanTypeEntityConfiguration());
            modelBuilder.ApplyConfiguration(new ServiceCategoryEntityConfiguration());
        }
    }
}
