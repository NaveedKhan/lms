﻿using BOL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace BOL.DBContext
{
    public class AppDBContext :  IdentityDbContext<ApplicationUser, IdentityRole<int>, int>  // IdentityDbContext<ApplicationUser>
    {
      
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }

       public DbSet<CustomerLocation> CustomerLocations { get; set; }
       public DbSet<AreaAssignedToDriver> AreaAssignedToDrivers { get; set; }
       public DbSet<Areas> Areas { get; set; }
       public DbSet<CustomersInArea> CustomersInAreas { get; set; }
       public DbSet<CustomersInAreaDeliveryOrder> CustomersInAreaDeliveryOrders { get; set; }
       public DbSet<Products> Products { get; set; }
       public DbSet<DeliverySchedule> DeliverySchedules { get; set; }
       public DbSet<DeliveryScheduleDetails> DeliveryScheduleDetails { get; set; }
       public DbSet<CustomerAssignedDays> CustomerAssignedDays { get; set; }



    }


    public static class Dbextension {

        public static void EnsureSeed(this AppDBContext db, IServiceProvider services,string notfounfimage, RoleManager<IdentityRole<int>> RoleManager,DataTable dt=null) {
            DataSeeder.Seeddata(db,notfounfimage,RoleManager);
        }
    }

    //this technique is used to seed data int database in core 2.1version using add migration
    //public static class ModelBuilderExtensions
    //{
    //    public static void Seed(this ModelBuilder modelBuilder)
    //    {
    //        modelBuilder.Entity<Department>().HasData(
    //            new Department { DepartmentID = 1, DepartmentName = "William" },
    //            new Department { DepartmentID = 2, DepartmentName = "William" },
    //            new Department { DepartmentID = 3, DepartmentName = "William" }
    //        );
         
    //    }
    //}
}
