﻿using BOL.Models;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;


namespace DAL.Interfaces
{
    public interface IRepositoryContext : IDbContext
    {
        DbSet<LoanType> LoanTypes { get; set; }
        DbSet<ServiceCategory> ServiceCategories { get; set; }
        DbSet<Products> Products { get; set; }
        DbSet<LoanService> LoanServices { get; set; }
        DbSet<LoanRequest> LoanRequests { get; set; }
        DbSet<LoanRepayment> LoanRepayments { get; set; }
        DbSet<ContactUs> ContactUs { get; set; }
        DbSet<FAQs> FAQs { get; set; }

    }
}