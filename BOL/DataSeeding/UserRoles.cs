﻿using BOL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BOL.DataSeeding
{
   static public class UserRoles
    {

        public static async Task CreateRoles(IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole<int>>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            string[] roleNames = { "Admin", "Employee", "HR", "User" };
            IdentityResult roleResult;
            //   "Electrition","Painter","Cleaner", "Home Shifting","Laundry"
            //Role area
            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    //create the roles and seed them to the database: Question 1  
                    roleResult = await RoleManager.CreateAsync(new IdentityRole<int>(roleName));
                }
            }

            ApplicationUser user = await UserManager.FindByEmailAsync("admin@gmail.com");

            if (user == null)
            {
                user = new ApplicationUser()
                {
                    FullName = "admin khan",
                    UserName = "admin@gmail.com",
                    Email = "admin@gmail.com",
                    RoleName = "Admin"
                };
                await UserManager.CreateAsync(user, "Test@123");
                await UserManager.AddToRoleAsync(user, "Admin");


            }

            ApplicationUser driverUser = await UserManager.FindByNameAsync("User1");

            if (driverUser == null)
            {
                user = new ApplicationUser()
                {
                    FullName = "User khan",
                    UserName = "User1",
                    Email = "User1@gmail.com",
                    RoleName = "User"
                };
                await UserManager.CreateAsync(user, "Test@123");
                await UserManager.AddToRoleAsync(user, "User");

            }
        }



    }
}
