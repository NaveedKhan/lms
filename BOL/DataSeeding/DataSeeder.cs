﻿using BOL.DBContext;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace BOL
{
    public static class DataSeeder
    {

        public static void Seeddata(AppDBContext dbContext, string notfoundImage, RoleManager<IdentityRole<int>> RoleManager)
        {


            using (var tran = dbContext.Database.BeginTransaction())
            {

                try
                {

                    //if (!dbContext.Areas.Any())
                    //{
                    //    dbContext.Areas.Add(new Areas { AreaName = "F11-F10 Area", IsActive = true });
                    //    dbContext.Areas.Add(new Areas { AreaName = "G11-G10 Area", IsActive = true });
                    //    dbContext.Areas.Add(new Areas { AreaName = "E11-E10 Area", IsActive = true });
                    //    dbContext.SaveChanges();
                    //}


                    tran.Commit();
                }
                catch (System.Exception ex)
                {
                    tran.Rollback();
                    throw;
                }
            }
        }


    }
}
