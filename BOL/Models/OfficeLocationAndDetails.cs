﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BOL.Models
{
    public class OfficeLocationAndDetails
    {
        [Key]
        public int OfficeID { get; set; }
        public string OfficeName { get; set; }
        public bool IsActive { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string ShortAddress { get; set; }
        public string FormatedAddress { get; set; }
        public string PlaceName { get; set; }
        public string SectorName { get; set; }
        public string BlockName { get; set; }
        public string RegionName { get; set; }
        public string StreetNo { get; set; }
    }
}
