﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BOL.Models
{
    public class LoanRequest
    {
        [Key]
        public int LoanRequestID { get; set; }
        public string LoanRequeteeName { get; set; }
        public string LoanRequesteeContactNo { get; set; }
        public string LoanRequesteeEmail { get; set; }
        public int LoanRequestStatus { get; set; }
        public bool IsPaid { get; set; }
        public DateTime LoanRequestDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public int LoanRequestAmount { get; set; }
        public int FkLoanServiceId { get; set; }
        public int FkUserId { get; set; }
        public string AdditionalComments { get; set; }
        public LoanService LoanService { get; set; }
        public ApplicationUser User { get; set; }
        public List<LoanRepayment> LoanRepayments { get; set; }

    }
}
