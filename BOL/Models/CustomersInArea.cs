﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BOL.Models
{
   public class CustomersInArea
    {
        [Key]
        public int CustomersInAreaID { get; set; }


        public int CustomerID { get; set; }
        [ForeignKey("CustomerID")]
        public ApplicationUser ApplicationUser { get; set; }

        public int AreaID { get; set; }
        [ForeignKey("AreaID")]
        public Areas Areas { get; set; }

    }
}
