﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BOL.Models
{
   public class DeliveryScheduleDetails
    {
        [Key]
        public int DeliveryScheduleDetailsID { get; set; }
        public bool IsActive { get; set; }
        public int CustomerID { get; set; }
        [ForeignKey("CustomerID")]
        public ApplicationUser ApplicationUser { get; set; }

        public int DeliveryScheduleID { get; set; }
        [ForeignKey("DeliveryScheduleID")]
        public DeliverySchedule DeliverySchedule { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }

        public DateTime DeliveryDate { get; set; }
        public DateTime DeliveredAt { get; set; }
        public bool IsDelivered { get; set; }
        public int NoOfBottles { get; set; }
        public int BottlePrice { get; set; }
        public int PaidAmount { get; set; }
        public int TotalAmount { get; set; }
        public bool IsPayementPaid { get; set; }
        public bool IsRushCall { get; set; }
        public bool IsPartialPayementPaid { get; set; }
        public bool IsCancelled { get; set; }
        public string CancelReason { get; set; }

    }
}
