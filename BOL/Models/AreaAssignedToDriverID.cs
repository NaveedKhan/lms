﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BOL.Models
{
   public class AreaAssignedToDriver
    {
        [Key]
        public int AreaAssignedToDriverID { get; set; }


        public int DriverID { get; set; }
        [ForeignKey("DriverID")]
        public ApplicationUser ApplicationUser { get; set; }

        public int AreaID { get; set; }
        [ForeignKey("AreaID")]
        public Areas Areas { get; set; }

    }
}
