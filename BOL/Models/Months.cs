﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BOL.Models
{
   public class Months
    {
        public int MonthID { get; set; }
        public string MonthName { get; set; }
    }
}
