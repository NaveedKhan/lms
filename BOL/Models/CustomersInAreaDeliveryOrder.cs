﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BOL.Models
{
   public class CustomersInAreaDeliveryOrder
    {
        [Key]
        public int CustomersInAreaDeliveryOrderID { get; set; }


        public int CustomerID { get; set; }
        [ForeignKey("CustomerID")]
        public ApplicationUser ApplicationUser { get; set; }

        public int AreaID { get; set; }
        [ForeignKey("AreaID")]
        public Areas Areas { get; set; }

        public int DayID { get; set; }
        public int OrderNo    { get; set; }
        public double DistanceFromPreviousStop { get; set; }
        public double Distance { get; set; }
        public double Duration { get; set; }

    }
}
