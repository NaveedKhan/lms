﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BOL.Models
{
    public class LoanService
    {
        [Key]
        public int LoanServiceID { get; set; }
        public string LoanServiceName { get; set; }
        public int FkLoanTypeId { get; set; }
        public int FkServiceCategoryId { get; set; }
        public string ImageUrl { get; set; }
        public int MaxLoanAmount { get; set; }
        public string LoanServiceDescription { get; set; }
        public bool IsActive { get; set; }
        public LoanType LoanType { get; set; }
        public ServiceCategory ServiceCategory { get; set; }
        public List<LoanRequest> LoanRequests { get; set; }
    }
}
