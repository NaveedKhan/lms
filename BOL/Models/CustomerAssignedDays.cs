﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BOL.Models
{
    public class CustomerAssignedDays
    {
        [Key]
        public int CustomerAssignedDaysID { get; set; }


        public int CustomerID { get; set; }
        [ForeignKey("CustomerID")]
        public ApplicationUser ApplicationUser { get; set; }

        public int Day { get; set; }
        public string DayName { get; set; }

    }
}
