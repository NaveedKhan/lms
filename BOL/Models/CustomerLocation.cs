﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BOL.Models
{
    public class CustomerLocation
    {
        [Key]
        public int CustomerLocationID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string ShortAddress { get; set; }
        public string FrormatedAddress { get; set; }
        public string PlaceName { get; set; }
        public string SectorName { get; set; }
        public string BlockName { get; set; }
        public string RegionName { get; set; }


        public int CustomerID { get; set; }
        [ForeignKey("CustomerID")]
        public ApplicationUser ApplicationUser { get; set; }
    }
}
