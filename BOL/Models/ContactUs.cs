﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BOL.Models
{
    public class ContactUs
    {
        [Key]
        public int ContactUsID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string CompanyName { get; set; }
        public string Message { get; set; }
    }
}
