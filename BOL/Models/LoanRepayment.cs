﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BOL.Models
{
   public class LoanRepayment
    {
        [Key]
        public int LoanRepaymentID { get; set; }
        public int FkLoanRequestID { get; set; }
        public DateTime PaymentDate { get; set; }
        public int TransactionAmount { get; set; }
        public int PaymentMethod { get; set; }
        public string CardDetails { get; set; }
        public LoanRequest LoanRequest { get; set; }
    }
}
