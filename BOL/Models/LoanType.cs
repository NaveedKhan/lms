﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BOL.Models
{
    public class LoanType
    {
        [Key]
        public int LoanTypeID { get; set; }
        public string LoanTypeName { get; set; }
        public bool IsActive { get; set; }
        public List<LoanService> LoanServices { get; set; }
    }
}
