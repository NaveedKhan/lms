﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BOL.Models
{
    public class ServiceCategory
    {
        [Key]
        public int ServiceCategoryID { get; set; }
        public string ServiceCategoryName { get; set; }
        public bool IsActive { get; set; }

        public List<LoanService> LoanServices { get; set; }
    }
}
