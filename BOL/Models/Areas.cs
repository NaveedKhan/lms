﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BOL.Models
{
   public class Areas
    {
        [Key]
        public int AreaID { get; set; }
        public string AreaName { get; set; }
        public bool IsActive { get; set; }
        public string AssignedDriverName { get; set; }
        public string NoOfCusotomers { get; set; }
    }
}
