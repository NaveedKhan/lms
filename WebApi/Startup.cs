using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Classes;
using BLL.Interfaces;
using BLL.Mapper;
using BOL.DataSeeding;
using BOL.DBContext;
using BOL.Models;
using DAL.Classes;
using DAL.Interfaces;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace DeosaiWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<AppDBContext>(options =>
                  options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            services.AddIdentity<ApplicationUser, IdentityRole<int>>()
            .AddEntityFrameworkStores<AppDBContext>()
            //.AddDefaultUI()
            .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.Cookie.Name = "Deosai";
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                options.LoginPath = "/Identity/Account/Login";
                // ReturnUrlParameter requires 
                //using Microsoft.AspNetCore.Authentication.Cookies;
                options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                options.SlidingExpiration = true;
            });

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                //options.Password.RequiredLength = 6;
                //options.Password.RequiredUniqueChars = 1;


                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;
            });

            services.AddControllers();

            services.AddMvc()
           .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.ConfigureRepositoryWrapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider services)
        {
            //do data seed here 
            var notfoundImage = Configuration.GetValue<string>("UserData:404ImagePath");
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var RoleManager = services.GetRequiredService<RoleManager<IdentityRole<int>>>();
                UserRoles.CreateRoles(services).Wait();
                serviceScope.ServiceProvider.GetService<AppDBContext>().EnsureSeed(services, notfoundImage, RoleManager);
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
    public static class ServiceExt
    {

        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            // register repositpry 
            services.AddScoped<IAreaRepository, AreaRepository>();
            services.AddScoped<IRepositoryContext, AppDBContext>();
            services.AddScoped<ICustomersInAreaRepositry, CustomersInAreaRepository>();
            services.AddScoped<ICustomerLocationRepository, CustomerLocationRepository>();
            services.AddScoped<IUsersRepository, UserRepository>();
            services.AddScoped<ICustomerAssignedDaysRepository, CustomerAssignedDaysRepository>();
            services.AddScoped<ICustomersInAreaDeliveryOrderRepository, CustomersInAreaDeliveryOrderRepository>();
            services.AddScoped<IDeliveryScheduleRepository, DeliveryScheduleRepository>();
            services.AddScoped<IDeliveryScheduleDetailsRepository, DeliveryScheduleDetailsRepository>();
            services.AddScoped<IAreaAssignedToDriverRepository, AreaAssignedToDriverRepository>();
            services.AddScoped<IOfficeLocationAndDetailsRepository, OfficeLocationAndDetailsRepository>();
            services.AddScoped<IUnitOfWork>(unitOfWork => new UnitOfWork(unitOfWork.GetService<IRepositoryContext>()));

            //services mapper
            var config = ModelMapper.Configure();
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            // register implemented services
            services.AddScoped<ISecurityService, SecurityService>();
            services.AddScoped<IAreaService, AreaService>();
            services.AddScoped<ICustomersInAreaService, CustomersInAreaService>();
            services.AddScoped<ICustomerLocationService, CustomerLocationService>();
            services.AddScoped<IUserServices, UserServices>();
            services.AddScoped<ICustomerAssignedDaysService, CustomerAssignedDaysService>();
            services.AddScoped<ICustomersInAreaDeliveryOrderService, CustomersInAreaDeliveryOrderService>();
            services.AddScoped<IDeliveryScheduleService, DeliveryScheduleService>();
            services.AddScoped<IDeliveryScheduleDetailsService, DeliveryScheduleDetailsService>();
            services.AddScoped<IAreaAssignedToDriverService, AreaAssignedToDriverService>();
            services.AddScoped<IDistanceCalculationService, DistanceCalculationService>();
            services.AddScoped<IOfficeLocationAndDetailsService, OfficeLocationAndDetailsService>();
        }
    }
}
