﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.ViewModels;
using BOL.DBContext;
using BOL.Models;
using DAL.Classes;
using DAL.DataTableToLists;
using DAL.DirectDbConnection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DeosaiWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly ILogger<AuthController> _logger;
        private readonly AppDBContext _context;
        private readonly UserManager<ApplicationUser> _usermanager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public IConfiguration Configuration { get; }


        public AuthController(ILogger<AuthController> logger,
            IConfiguration configuration,
            AppDBContext context, UserManager<ApplicationUser> usermanager,
            SignInManager<ApplicationUser> signInManager)
        {
            _logger = logger;
            Configuration = configuration;
            _context = context;
            _usermanager = usermanager;
            _signInManager = signInManager;
        }

        [HttpPost("LoginDrivers")]
        public async Task<ActionResult<object>> LoginDrivers(LoginViewModel model)
        {
            var logmessagge = new StringBuilder();
            var resul2t = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, true, false);
            var userRolesArray = new List<string>();
            if (resul2t.Succeeded)
            {
                logmessagge.AppendLine($"login success");
                var user = _context.Users.FirstOrDefault(sa => sa.UserName == model.UserName);
                var deviceId = model.DeviceID;
                var deviceToken = model.DeviceTocken;

                var roles = await _usermanager.GetRolesAsync(user);
                userRolesArray = roles.ToList();

                if (user != null)
                {
                    user.RoleName = string.Join(",", userRolesArray);
                }
                var userid = user.Id;
                logmessagge.AppendLine($"user id {userid}");

                if (userRolesArray.Contains("Driver"))
                {
                    var driverArea = _context.AreaAssignedToDrivers.Where(aa => aa.DriverID == userid).ToArray(); ;
                    logmessagge.AppendLine($"assign area {driverArea.Select(aa => aa.AreaID)}");

                    if (driverArea.Length > 0)
                    {
                        var currentDate = DateTime.Now;
                        var areaId = driverArea.Select(aa => aa.AreaID).ToList();// driverArea.AreaID;
                        var currentMonthdeliverySchedule = _context.DeliverySchedules.FirstOrDefault(aa => aa.Month == currentDate.Month
                     && aa.Year == currentDate.Year);
                        if (currentMonthdeliverySchedule == null)
                            return new
                            {
                                Data = new { },
                                Message = "Schedule is not created for this month",
                                Success = false
                            };

                        var stringdate = currentDate.ToString("yyyy-MM-dd");
                        var connectionString = Configuration.GetConnectionString("DefaultConnection");
                        var query = $@"select  
                                     dsd.DeliveryScheduleID,
                                     dsd.DeliveryScheduleDetailsID,
                                     dsd.DeliveryDate,
                                     customer.FullName,
                                     customer.PhoneNumber,
                                     customer.Address,
                                     dsd.CustomerID,
                                     dsd.NoOfBottles,
                                     dsd.PaidAmount,
                                     dsd.IsDelivered,
                                     dsd.IsCancelled,
                                     dsd.IsPayementPaid,
                                     dsd.DeliveredAt,
                                     dsd.IsPartialPayementPaid,
                                     dsd.Day,
                                     dsd.Month,
                                     dorder.DistanceFromPreviousStop,
                                     dorder.OrderNo,
                                     custLoc.Latitude,
                                     custLoc.Longitude,
                                     custLoc.ShortAddress,
                                     custLoc.FrormatedAddress,
                                     dsd.BottlePrice
                                     from 
                                     DeliveryScheduleDetails dsd 
                                     inner join AspNetUsers customer on dsd.CustomerID = customer.Id
                                     inner join CustomerLocations custLoc on custLoc.CustomerID = customer.Id
                                     inner join CustomersInAreaDeliveryOrders dorder on customer.Id = dorder.CustomerID and dorder.DayID = {(int)currentDate.DayOfWeek}
                                     where dsd.DeliveryScheduleID = {currentMonthdeliverySchedule.DeliveryScheduleID} and dsd.DeliveryDate like '%{stringdate}%'
                                    and dsd.CustomerID in (
                                    select CustomersInAreas.CustomerID from CustomersInAreas where AreaID in
                                    ({string.Join(",", areaId)}))  order by dorder.OrderNo;";
                        var result = DirectQueries.ExecuteQuery_DT(query, connectionString);

                        var Mlist = new List<DeliveryScheduleDetailsVM>();
                        try
                        {
                            Mlist = ConvertDTToList.ConvertDataTable<DeliveryScheduleDetailsVM>(result);
                        }
                        catch (Exception ex)
                        {
                            return new
                            {
                                Data = new
                                {
                                     
                                },
                                Message = ex.Message,
                                Success = false
                            };
                        }
                        logmessagge.AppendLine($"currentMonthdeliverySchedule is {currentMonthdeliverySchedule.DeliveryScheduleID}");

                        return new
                        {
                            Data = new
                            {
                                user,
                                relatedOrders = Mlist,
                                unReadnotificationCount = 0
                            },
                            Message = "Login Success " + string.Join(",", userRolesArray),
                            Success = true
                        };
                    }
                    else
                    {
                        return new { Data = new { }, Message = "No Area assigned to Driver yet.", Success = false };
                    }
                }
                else
                {
                    return new { Data = new { }, Message = "neither grocerry driver , nor handy worker " + string.Join(",", userRolesArray), Success = false };
                }

            }
            else
            {
                return new { Data = new { }, Message = "Invalid username or password", Success = false };
            }
        }

        [HttpGet("SignOut")]
        public async Task<object> SignOut()
        {
            try
            {
                await _signInManager.SignOutAsync();
                return new
                {
                    Message = "Account Created for mobile user",
                    Success = true
                };
            }
            catch (Exception ex)
            {

                return new
                {
                    Message = $"logout falied {ex.Message}",
                    Success = false
                };
            }
        }
    }
}
