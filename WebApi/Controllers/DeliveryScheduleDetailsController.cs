﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOL.DBContext;
using BOL.Models;
using BLL.ViewModels;
using DAL.DirectDbConnection;
using DAL.DataTableToLists;
using Microsoft.Extensions.Configuration;
using DAL.Classes;

namespace DeosaiWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveryScheduleDetailsController : ControllerBase
    {
        private readonly AppDBContext _context;
        public IConfiguration Configuration { get; }

        public DeliveryScheduleDetailsController(AppDBContext context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }

        // GET: api/DeliveryScheduleDetails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DeliveryScheduleDetails>>> GetDeliveryScheduleDetails()
        {
            return await _context.DeliveryScheduleDetails.ToListAsync();
        }

        // GET: api/DeliveryScheduleDetails/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DeliveryScheduleDetails>> GetDeliveryScheduleDetails(int id)
        {
            var deliveryScheduleDetails = await _context.DeliveryScheduleDetails.FindAsync(id);

            if (deliveryScheduleDetails == null)
            {
                return NotFound();
            }

            return deliveryScheduleDetails;
        }

        [HttpGet("GetCurrentDeliveryScheduleDetailsForDriver/{id}")]
        public async Task<ActionResult<object>> GetCurrentDeliveryScheduleDetailsForDriver(int id)
        {
            var driverArea =await _context.AreaAssignedToDrivers.Where(aa => aa.DriverID == id).ToListAsync();
            if (driverArea != null)
            {
                var areaId = driverArea.Select(aa => aa.AreaID).ToList();
                var currentDate = DateTime.Now;
                var currentMonthdeliverySchedule = _context.DeliverySchedules.FirstOrDefault(aa => aa.Month == currentDate.Month
                && aa.Year == currentDate.Year);
                if (currentMonthdeliverySchedule == null)
                    return new
                    {
                        Data = new { },
                        Message = "Schedule is not created for this month",
                        Success = false
                    };
                var stringdate = currentDate.ToString("yyyy-MM-dd");
                var connectionString = Configuration.GetConnectionString("DefaultConnection");
                var query = $@"select  
                                     dsd.DeliveryScheduleID,
                                     dsd.DeliveryScheduleDetailsID,
                                     dsd.DeliveryDate,
                                     customer.FullName,
                                     customer.PhoneNumber,
                                     customer.Address,
                                     dsd.CustomerID,
                                     dsd.IsDelivered,
                                     dsd.IsCancelled,
                                     dsd.NoOfBottles,
                                     dsd.DeliveredAt,
                                     dsd.PaidAmount,
                                     dsd.IsPayementPaid,
                                     dsd.IsPartialPayementPaid,
                                     dsd.Day,
                                     dsd.Month,
                                     dorder.DistanceFromPreviousStop,
                                     dorder.OrderNo,
                                     custLoc.Latitude,
                                     custLoc.Longitude,
                                     custLoc.ShortAddress,
                                     custLoc.FrormatedAddress,
                                     dsd.BottlePrice
                                     from 
                                     DeliveryScheduleDetails dsd 
                                     inner join AspNetUsers customer on dsd.CustomerID = customer.Id
                                     inner join CustomerLocations custLoc on custLoc.CustomerID = customer.Id
                                     inner join CustomersInAreaDeliveryOrders dorder on customer.Id = dorder.CustomerID and dorder.DayID = {(int)currentDate.DayOfWeek}
                                     where dsd.DeliveryScheduleID = {currentMonthdeliverySchedule.DeliveryScheduleID} and dsd.DeliveryDate like '%{stringdate}%'
                                     and dsd.CustomerID in (
                                    select CustomersInAreas.CustomerID from CustomersInAreas where AreaID in 
                                    ({string.Join(",", areaId)})) order by dorder.OrderNo;";
                var result = DirectQueries.ExecuteQuery_DT(query, connectionString);
                var Mlist = ConvertDTToList.ConvertDataTable<DeliveryScheduleDetailsVM>(result);

                return new
                {
                    Data = new
                    {
                        relatedOrders = Mlist,
                        unReadnotificationCount = 0
                    },
                    Message = "Dat Fetched ",
                    Success = true
                };
            }
            else
            {
                return new { Data = new { }, Message = "No Area assigned to Driver yet.", Success = false };
            }

        }

        [HttpPost("MarkDeliveryOrderAsComplete")]
        public async Task<ActionResult<object>> MarkDeliveryOrderAsComplete(DeliveryScheduleDetailsVM model)
        {

            var deliversc = _context.DeliveryScheduleDetails.FirstOrDefault(aa => aa.DeliveryScheduleDetailsID == model.DeliveryScheduleDetailsID);
            try
            {
                if (deliversc != null)
                {
                    deliversc.IsDelivered = true;
                    deliversc.NoOfBottles = model.NoOfBottles;
                    deliversc.TotalAmount = (model.NoOfBottles * deliversc.BottlePrice);
                    deliversc.DeliveredAt = DateTime.Now;
                    deliversc.IsPayementPaid = model.IsPayementPaid;
                    deliversc.PaidAmount = model.PaidAmount;
                    await _context.SaveChangesAsync();
                    return new { Data = new { }, Message = "Updated.", Success = true };
                }
                else
                {
                    return new { Data = new { }, Message = "No Area assigned to Driver yet.", Success = false };
                }
            }
            catch (Exception ex)
            {

                return new { Data = new { }, Message = "exception " + ex.Message, Success = false };
            }


        }



        [HttpPost("MarkDeliveryOrderAsCancel")]
        public async Task<ActionResult<object>> MarkDeliveryOrderAsCancel(DeliveryScheduleDetailsVM model)
        {

            var deliversc = _context.DeliveryScheduleDetails.FirstOrDefault(aa => aa.DeliveryScheduleDetailsID == model.DeliveryScheduleDetailsID);
            try
            {
                if (deliversc != null)
                {
                    deliversc.IsDelivered = false;
                    deliversc.IsCancelled = true;
                    deliversc.CancelReason = model.CancelReason;
                    deliversc.DeliveredAt = DateTime.Now;
                    await _context.SaveChangesAsync();
                    return new { Data = new { }, Message = "Updated.", Success = true };
                }
                else
                {
                    return new { Data = new { }, Message = "No Area assigned to Driver yet.", Success = false };
                }
            }
            catch (Exception ex)
            {

                return new { Data = new { }, Message = "exception " + ex.Message, Success = false };
            }


        }



        // PUT: api/DeliveryScheduleDetails/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDeliveryScheduleDetails(int id, DeliveryScheduleDetails deliveryScheduleDetails)
        {
            if (id != deliveryScheduleDetails.DeliveryScheduleDetailsID)
            {
                return BadRequest();
            }

            _context.Entry(deliveryScheduleDetails).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DeliveryScheduleDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DeliveryScheduleDetails
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<DeliveryScheduleDetails>> PostDeliveryScheduleDetails(DeliveryScheduleDetails deliveryScheduleDetails)
        {
            _context.DeliveryScheduleDetails.Add(deliveryScheduleDetails);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDeliveryScheduleDetails", new { id = deliveryScheduleDetails.DeliveryScheduleDetailsID }, deliveryScheduleDetails);
        }

        // DELETE: api/DeliveryScheduleDetails/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DeliveryScheduleDetails>> DeleteDeliveryScheduleDetails(int id)
        {
            var deliveryScheduleDetails = await _context.DeliveryScheduleDetails.FindAsync(id);
            if (deliveryScheduleDetails == null)
            {
                return NotFound();
            }

            _context.DeliveryScheduleDetails.Remove(deliveryScheduleDetails);
            await _context.SaveChangesAsync();

            return deliveryScheduleDetails;
        }

        private bool DeliveryScheduleDetailsExists(int id)
        {
            return _context.DeliveryScheduleDetails.Any(e => e.DeliveryScheduleDetailsID == id);
        }
    }
}
