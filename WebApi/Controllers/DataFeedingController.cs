﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.ViewModels;
using BOL.DBContext;
using BOL.Models;
using DAL.Classes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebApi.Helper;

namespace DeosaiWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataFeedingController : ControllerBase
    {
        private readonly ILogger<DataFeedingController> _logger;
        private readonly AppDBContext _context;
        private readonly UserManager<ApplicationUser> _usermanager;
        private readonly ICustomersInAreaDeliveryOrderService _customersInAreaDeliveryOrderService;
        public DataFeedingController(ILogger<DataFeedingController> logger, AppDBContext context, UserManager<ApplicationUser> usermanager, ICustomersInAreaDeliveryOrderService customersInAreaDeliveryOrderService)
        {
            _logger = logger;
            _context = context;
            _usermanager = usermanager;
            _customersInAreaDeliveryOrderService = customersInAreaDeliveryOrderService;
        }

        [HttpGet("index")]
        public async Task<object> Index() 
        {
            await _customersInAreaDeliveryOrderService.SetordersOfdeliveriesByAreaID(7, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            //await CalculateDistanceUsingDistanceMatrixApi();
            return new { success = true };
        }

        // GET: api/Areas
        [HttpGet("api/[controller]/AddUsets")]
        public async Task<ActionResult<object>> AddUsers()
        {
            //var requestss = "https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyADPwys5xIOIM0h_LB3wPI8p3A5er5tSqs";
            //HttpClient client = new HttpClient();
            //HttpResponseMessage response = await client.GetAsync(requestss);
            //response.EnsureSuccessStatusCode();
            //string responseBody = await response.Content.ReadAsStringAsync();
            //var json2 = JsonConvert.DeserializeObject<GoogleGeoCodeResponse>(responseBody);


            // DateTime dt2 = firstDayOfMonth.StartOfWeek(DayOfWeek.Monday);
            var isdataAdded = _context.Users.FirstOrDefault(aa => aa.FullName == "Abdul Qadir= C/o KMK G-10/1");
            if (isdataAdded == null)
            {
                var userslist = ExcelHeper.ReadData();
                foreach (var item in userslist)
                {
                    var result = await CreateIdentityUser(item);
                }
            }

            return new
            {
                Data = new { },
                orgid = 0,
                Message = "Hogiya",
                Success = true
            };
        }

        [HttpGet("AddAddressesDetail")]
        public async Task<ActionResult<object>> AddAddressesDetail()
        {

            var userDoeshaveAddresses = _context.CustomerLocations.Select(aa => aa.CustomerID).ToList();
            var userDoesNotHaveAddresses = _context.Users.Where(aa => !userDoeshaveAddresses.Contains(aa.Id) &&
            aa.RoleName == "DeliveryCustomer").AsEnumerable();
            foreach (var item in userDoesNotHaveAddresses)
            {
                var addressSet = item.Address.Replace(" ", "+");
                var googleAPiCallheader = $"https://maps.googleapis.com/maps/api/geocode/json?address={addressSet}&key=AIzaSyADPwys5xIOIM0h_LB3wPI8p3A5er5tSqs";
                HttpClient client = new HttpClient();
                try
                {
                    HttpResponseMessage response = await client.GetAsync(googleAPiCallheader);
                    string responseBody = await response.Content.ReadAsStringAsync();
                    var json2 = JsonConvert.DeserializeObject<GoogleGeoCodeResponse>(responseBody);
                    if (json2.status.ToLower() == "ok")
                    {
                        var customerAdderss = new CustomerLocation
                        {
                            FrormatedAddress = json2.results.First().formatted_address,
                            ShortAddress = item.Address,
                            Latitude = json2.results.First().geometry.location.lat.ToString(),
                            Longitude = json2.results.First().geometry.location.lng.ToString(),
                            CustomerID = item.Id
                        };
                        _context.CustomerLocations.Add(customerAdderss);
                    }
                    else
                    {
                    }
                }
                catch (Exception ex)
                {
                    continue;
                }


            }

            await _context.SaveChangesAsync();
            return new
            {
                Data = new { },
                orgid = 0,
                Message = "Hogiya",
                Success = true
            };
        }

        public async Task CalculateDistanceUsingDistanceMatrixApi()
        {
            var userDoeshaveAddresses = _context.CustomerLocations.Select(aa => aa.CustomerID).ToList();
            var userDoesNotHaveAddresses = _context.Users.Where(aa => !userDoeshaveAddresses.Contains(aa.Id) &&
            aa.RoleName == "DeliveryCustomer").AsEnumerable();
            
               // var addressSet = item.Address.Replace(" ", "+");
                var matrixUrl = $"https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=40.6655101,-73.89188969999998&destinations=40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.659569%2C-73.933783%7C40.729029%2C-73.851524%7C40.6860072%2C-73.6334271%7C40.598566%2C-73.7527626%7C40.659569%2C-73.933783%7C40.729029%2C-73.851524%7C40.6860072%2C-73.6334271%7C40.598566%2C-73.7527626&key=AIzaSyADPwys5xIOIM0h_LB3wPI8p3A5er5tSqs";
                HttpClient client = new HttpClient();

                try
                {
                    HttpResponseMessage response = await client.GetAsync(matrixUrl);
                    string responseBody = await response.Content.ReadAsStringAsync();
                    var json2 = JsonConvert.DeserializeObject<DistanceMatrixResult>(responseBody);
                    if (json2.status.ToLower() == "ok")
                    {


                    }
                    else
                    {
                    }
                }
                catch (Exception ex)
                {
                    //continue;
                }
            
        }


        [HttpGet("CalculateDistance")]
        public async Task<ActionResult<object>> CalculateDistance()
        {
            CalculateDistances();
            return new { success = true };
        }

        private void CalculateDistances()
        {
            var deosaiOffice = new DistanceCalculation { lat = 33.6636623071753, lng = 73.04966644747465, distanceFromOffice = 0.0 };
            var previousStop = deosaiOffice;
            var sortedList = new List<DistanceCalculation>();
            var order = 0;

            //var AreasWiseUsers = _context.CustomersInAreas.GroupBy(aa => aa.AreaID).ToList();
            var customerInArea = _context.CustomersInAreas.Where(sa => sa.AreaID == 2).Select(aa => aa.CustomerID).ToList();
            var locationLists = _context.CustomerLocations.Where(sa => customerInArea.Contains(sa.CustomerID))
                .Select(aa => new DistanceCalculation
                {
                    lat = Convert.ToDouble(aa.Latitude),
                    lng = Convert.ToDouble(aa.Longitude),
                    customerId = aa.CustomerID,
                    locationid = aa.CustomerLocationID,
                    address = aa.ShortAddress,
                    distanceFromOffice = 0.0,
                    distanceFromPreviosStop = 0.0,
                    order = 0
                }).ToList();

            while (locationLists.Count > 0)
            {
                locationLists = SortByDistance(previousStop, locationLists);
                var nearestStop = locationLists.OrderBy(aa => aa.distanceFromPreviosStop).FirstOrDefault();
                nearestStop.order = ++order;
                sortedList.Add(nearestStop);
                previousStop = nearestStop;
                locationLists.Remove(nearestStop);
            }

            foreach (var locaction in sortedList)
            {
                _context.CustomersInAreaDeliveryOrders.Add(new CustomersInAreaDeliveryOrder
                {
                    AreaID = 1,
                    CustomerID = locaction.customerId,
                    DistanceFromPreviousStop = locaction.distanceFromPreviosStop,
                    OrderNo = locaction.order
                });
            }
            _context.SaveChanges();
        }

        private List<DistanceCalculation> SortByDistance(DistanceCalculation previousStop, List<DistanceCalculation> locationLists)
        {
            foreach (var item in locationLists)
            {
                var distance = new Coordinates(previousStop.lat, previousStop.lng)
                  .DistanceTo(
                      new Coordinates(item.lat, item.lng),
                      UnitOfLength.Kilometers
                  );
                item.distanceFromOffice = distance;
                item.distanceFromPreviosStop = distance;
                // regionCalc.Add(regionEnt);
            }


            return locationLists = locationLists.OrderBy(aa => aa.distanceFromPreviosStop).ToList();
        }

        #region Private Methods

        private async Task<IdentityResult> CreateIdentityUser(ApplicationUser userInfo)
        {

            var user = new ApplicationUser
            {
                UserName = userInfo.UserName,
                Email = userInfo.Email,
                FullName = userInfo.FullName,//$"{userInfo.FirstName} {userInfo.LastName}",
                PasswordHash = userInfo.PlainPassword,
                RoleName = userInfo.RoleName,
                PhoneNumber = userInfo.PhoneNumber,
                PlainPassword = userInfo.PlainPassword,
                Joiningdate = DateTime.Now,
                BottleSize = userInfo.BottleSize,
                DeliveryFrequency = userInfo.DeliveryFrequency,
                BottleSpecialPrice = userInfo.BottleSpecialPrice,
                Address = userInfo.Address
            };
            var result = await _usermanager.CreateAsync(user, userInfo.PlainPassword);

            if (result.Succeeded)
            {
                await _usermanager.AddToRoleAsync(user, user.RoleName);
                return result;
            }

            return result;
        }
        #endregion
    }
}
